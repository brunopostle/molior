#!/usr/bin/perl

use strict;
use warnings;
use Test::More 'no_plan';
use 5.010;

use lib ('lib', '../lib');
use Molior::DXF;

my $self = Molior::DXF->new;
$self->Read ('t/data/insert.molior');

$self->Read_Style();
$self->Read_Inserts();

is ($self->Type, 'molior-insert', 'Type()');

ok (my @dxf = $self->Insert_insert, 'Insert_Insert()');
ok ($self->Insert_in_use, 'Insert_in_use()');

like ($self->Insert_in_use, '/post_[0-9]+.dxf$/', 'Insert_in_use()');

like ($self->Insert_in_use, '/245.dxf$/', 'post is less than 2.5m');
$self->{space}->[1]->[2] = 2.4;
like ($self->Insert_in_use, '/230.dxf$/', 'post is less than 2.4m');
$self->{space}->[1]->[2] = 2.3;
like ($self->Insert_in_use, '/230.dxf$/', 'post is exactly 2.3m');
$self->{space}->[1]->[2] = 2.2;
like ($self->Insert_in_use, '/215.dxf$/', 'post is less than 2.2m');
$self->{space}->[1]->[2] = 2.1;
like ($self->Insert_in_use, '/200.dxf$/', 'post is less than 2.1m');
$self->{space}->[1]->[2] = 2.0;
like ($self->Insert_in_use, '/200.dxf$/', 'post is exactly 2.0m');
$self->{space}->[1]->[2] = 1.9;
like ($self->Insert_in_use, '/200.dxf$/', 'post is 2.0m even though we asked for 1.9m');

$self->{name} = 'beam column';
like ($self->Insert_in_use, '/column_[0-9]+.dxf$/', 'Insert_in_use()');

0;
