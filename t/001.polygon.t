#!/usr/bin/perl

use strict;
use warnings;
use Test::More 'no_plan';

use lib ('lib', '../lib');
use File::DXF::Math qw /distance_2d angle_2d subtract_2d add_2d scale_2d normalise_2d/;
use Molior;

my $wall = Molior->new;

my $temp = $wall->new;

#$wall->{closed} = 0;
#$wall->{elevation} = 0.0;
$wall->{path} = [[2,6],[6,1],[10,4],[9,7],[15,9]];
#$wall->{openings} = [];

$wall->Write ('foo.molior');
ok (-e 'foo.molior', 'output file exists');
unlink 'foo.molior';

is (distance_2d ([0,0], [3,4]), 5, 'distance_2d()');
is (distance_2d ([0,0], [-3,4]), 5, 'distance_2d()');
is (distance_2d ([0,0], [3,-4]), 5, 'distance_2d()');
is (distance_2d ([3,4], [7,7]), 5, 'distance_2d()');

is (angle_2d ([0,0],[9,0]) +0, 0, 'angle_2d()');
like (angle_2d ([0,0],[9,9]), '/0.7853/', 'angle_2d()');
is_deeply (subtract_2d ([5,4], [2,3]), [3,1], 'subtract_2d()');
is_deeply (add_2d ([5,4], [2,1]), [7,5], 'add_2d()');
is_deeply (scale_2d (2, [5,4]), [10,8], 'scale_2d()');
is_deeply (scale_2d ([5,4], 2), [10,8], 'scale_2d()');
is_deeply (normalise_2d ([3,4]), [0.6,0.8], 'normalise_2d()');
is_deeply (normalise_2d ([-3,4]), [-0.6,0.8], 'normalise_2d()');

0;
