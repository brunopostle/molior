#!/usr/bin/perl

use strict;
use warnings;
use Carp;
use Test::More 'no_plan';
use File::DXF;
use File::DXF::ENTITIES;
use File::DXF::HEADER;
use File::DXF::BLOCKS;

use lib ('lib', '../lib');
use Molior::DXF;

my $wall = Molior::DXF->new;
$wall->Read ('t/data/wall-closed.molior');
$wall->Read_Db();
ok ($wall->Read_Style(), 'Read_Style() success');

my $dxf = File::DXF->new;
$dxf->{HEADER} = File::DXF::HEADER->new;
$dxf->{ENTITIES} = File::DXF::ENTITIES->new;
$dxf->{BLOCKS} = File::DXF::BLOCKS->new;

push @{$dxf->{ENTITIES}}, $wall->Walls;
ok ($dxf->{BLOCKS}->Add_From_Disk ($wall->Openings_in_use), 'Add_From_Disk()');
ok (!$wall->Insert_in_use, 'Insert_in_use()');
ok ($dxf->{BLOCKS}->Add_From_Disk ($wall->Insert_in_use), 'Add_From_Disk()');

open my $DXF, '>', 'baz.dxf' or croak "$!";
print $DXF $dxf->DXF;
close $DXF;

ok (-e 'baz.dxf', 'file exists');
unlink 'baz.dxf';

0;
