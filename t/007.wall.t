#!/usr/bin/perl

use strict;
use warnings;
use Test::More 'no_plan';
use 5.010;

use lib ('lib', '../lib');
use Molior;

my $wall = Molior->new;
$wall->Read ('t/data/wall-closed.molior');
$wall->Read_Style();
$wall->Read_Db();

is ($wall->Type, 'molior-wall', 'Type()');

is ($wall->Segments, 12, 'Segments()');
is ($wall->{outer}, 0.25, 'outer');

is ($wall->{openings}->[0]->[1]->{up}, undef, 'height');
ok ($wall->Fix_Heights (0), 'Fix_Heights');
is ($wall->{openings}->[0]->[1]->{up}, 1.05, 'lower');

for (0 .. $wall->Segments -1)
{
    ok ($wall->Fix_Heights ($_), 'Fix_Heights()');
    ok ($wall->Fix_Segment ($_), 'Fix_Segment()');
}

0;
