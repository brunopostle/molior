#!/usr/bin/perl

use strict;
use warnings;
use Test::More 'no_plan';

use lib ('lib', '../lib');
use Molior::DXF;

my $self = Molior::DXF->new;
$self->Read ('t/data/extrusion.molior');
is ($self->Type, 'molior-extrusion', 'Type()');

ok (my @dxf = $self->Extrusion, 'Extrusion()');

$self->Read ('t/data/intermittent.molior');
is ($self->Type, 'molior-intermittent', 'intermittent');

ok ((push @dxf, $self->Intermittent), 'Intermittent()');

0;
