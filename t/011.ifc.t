#!/usr/bin/perl

use strict;
use warnings;
use Test::More 'no_plan';

use lib ('lib', '../lib');
use_ok ('Molior::IFC');
use_ok ('File::IFC');

my $molior = Molior::IFC->new;
$molior->Read ('t/data/triangle.molior');
is ($molior->Type, 'molior-wall', 'molior-wall');
ok (!$molior->Is_Quadrilateral, 'not quad');

my $ifc = File::IFC->new;

#$molior->Floor;
#$molior->Ceiling;
#$molior->Walls;

0;
