#!/usr/bin/perl

use strict;
use warnings;
use Test::More 'no_plan';

use lib ('lib', '../lib');
use Molior::DXF;

my $stair = Molior::DXF->new;
$stair->Read ('t/data/stair-3.molior');

is ($stair->Type, 'molior-stair', 'Type()');

is ($stair->Segments, 4, 'Segments()');
is ($stair->{inner}, 0.08, 'inner');
is ($stair->{width}, 1.25, 'width');

my @data = $stair->Stair;

ok ($stair->_is_in_use (0), '_is_in_use()');
ok (!$stair->_is_in_use (1), '_is_in_use()');
ok (!$stair->_is_in_use (2), '_is_in_use()');
ok ($stair->_is_in_use (3), '_is_in_use()');
ok ($stair->_is_in_use (4), '_is_in_use()');

0;
