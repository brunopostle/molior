#!/usr/bin/perl

use strict;
use warnings;
use Carp;
use Test::More 'no_plan';
use File::DXF;
use File::DXF::ENTITIES;
use File::DXF::HEADER;

use lib ('lib', '../lib');
use Molior::DXF;

my $self = Molior::DXF->new;
$self->Read ('t/data/triangle.molior');

is ($self->Type, 'molior-wall', 'Read()');

ok (!$self->Is_Quadrilateral, 'Is_Quadrilateral()');

$self->{elements}->[0]->[0] = [['0.0','0.0'],['4.0','0.0'],['4.0','1.6'],['1.0','1.5']];
$self->{elements}->[0]->[1] = '0101';

my $dxf = File::DXF->new;
$dxf->{HEADER} = File::DXF::HEADER->new;
$dxf->{ENTITIES} = File::DXF::ENTITIES->new;

push @{$dxf->{ENTITIES}}, $self->Floor;
push @{$dxf->{ENTITIES}}, $self->Ceiling;
push @{$dxf->{ENTITIES}}, $self->Monopitch (0, 2, 150/225);
push @{$dxf->{ENTITIES}}, $self->Duopitch (1, 2, 150/225);
push @{$dxf->{ENTITIES}}, $self->Monopitch (1, 0, 150/225);
push @{$dxf->{ENTITIES}}, $self->Duopitch (2, 1, 150/225);
ok ($self->Roof, 'Roof()');

open my $DXF, '>', '_test_quad.dxf' or croak "$!";
print $DXF $dxf->DXF;
close $DXF;

ok (-e '_test_quad.dxf', 'file exists');
unlink '_test_quad.dxf';

0;
