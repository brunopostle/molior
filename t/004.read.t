#!/usr/bin/perl

use strict;
use warnings;
use Carp;
use Test::More 'no_plan';
use File::DXF;
use File::DXF::ENTITIES;
use File::DXF::HEADER;
use File::DXF::BLOCKS;

use lib ('lib', '../lib');
use Molior::DXF;

my $wall = Molior::DXF->new;
ok (!$wall->Read (), 'read() fails');
ok (!$wall->Read ('does/not/exist'), 'Read(junk) fails');
ok ($wall->Read ('t/data/wall.molior'), 'Read(molior) success');
ok ($wall->Read_Db(), 'Read_Db() success');
ok ($wall->Read_Style(), 'Read_Style() success');

my $dxf = File::DXF->new;
$dxf->{HEADER} = File::DXF::HEADER->new;
$dxf->{ENTITIES} = File::DXF::ENTITIES->new;
$dxf->{BLOCKS} = File::DXF::BLOCKS->new;

push @{$dxf->{ENTITIES}}, $wall->Walls;
$dxf->{BLOCKS}->Add_From_Disk ($wall->Openings_in_use);

open my $DXF, '>', 'bar.dxf' or croak "$!";
print $DXF $dxf->DXF;
close $DXF;
ok (-e 'bar.dxf', 'file exists');
unlink 'bar.dxf';

0;
