#!/usr/bin/perl

use strict;
use warnings;
use Test::More 'no_plan';
use 5.010;

use lib ('lib', '../lib');
use Molior;

my $wall = Molior->new;

$wall->Read_Db();
$wall->Read_Style();

ok(defined $wall->{_db}->{'single inside door'}, '_db');
ok(defined $wall->{_db}->{'double inside door'}, '_db');

for my $style_name (keys %{$wall->{_style}->{opening}})
{
    my $opening_name = $wall->{_style}->{opening}->{$style_name}->{name};
    ok ($opening_name, "$style_name -> $opening_name");
    my $opening = $wall->{_db}->{$opening_name};
    ok (scalar (@{$opening}), "'$opening_name' is a list");
    for (@{$opening})
    {
        my $filename = $_->{file};
        ok (-e "lib/auto/Molior/$filename", "$filename exists");
    }
}

$wall->{style} = 'courtyard';
$wall->Read_Db();
$wall->Read_Style();

for my $style_name (keys %{$wall->{_style}->{opening}})
{
    my $opening_name = $wall->{_style}->{opening}->{$style_name}->{name};
    ok ($opening_name, "$style_name -> $opening_name");
    my $opening = $wall->{_db}->{$opening_name};
    ok (scalar (@{$opening}), "'$opening_name' is a list");
    for (@{$opening})
    {
        my $filename = $_->{file};
        ok ((-e "lib/auto/Molior/$filename" or -e "lib/auto/Molior/courtyard/$filename"), "$filename exists");
    }
}

my $molior = Molior->new;

$molior->{style} = '';
$molior->Read_Style();
$molior->Read_Inserts();
ok(defined $molior->{_insert}->{'post'}, '_insert');

for my $style_name (keys %{$molior->{_style}->{insert}})
{
    my $insert_name = $molior->{_style}->{insert}->{$style_name}->{name};
    ok ($insert_name, "$style_name -> $insert_name");
    my $insert = $molior->{_insert}->{$insert_name};
    ok (scalar (@{$insert}), "'$insert_name' is a list");
    for (@{$insert})
    {
        my $filename = $_->{file};
        ok (-e "lib/auto/Molior/$filename", "$filename exists");
    }
}

my $insert = $molior->Get_Insert ('pergola post');

ok(scalar @{$insert->{list}}, 'list is a list');
ok($insert->{type} eq 'stanchion', 'pergola post is a stanchion');


$molior->{style} = 'courtyard';
$molior->Read_Style();
$molior->Read_Inserts();
ok(defined $molior->{_insert}->{'post'}, '_insert');

for my $style_name (keys %{$molior->{_style}->{insert}})
{
    my $insert_name = $molior->{_style}->{insert}->{$style_name}->{name};
    ok ($insert_name, "$style_name -> $insert_name");
    my $insert = $molior->{_insert}->{$insert_name};
    ok (scalar (@{$insert}), "'$insert_name' is a list");
    for (@{$insert})
    {
        my $filename = $_->{file};
        ok (-e "lib/auto/Molior/$filename", "$filename exists");
    }
}

0;
