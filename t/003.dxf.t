#!/usr/bin/perl

use strict;
use warnings;
use 5.010;
use Carp;
use Test::More 'no_plan';
use File::DXF;
use File::DXF::ENTITIES;
use File::DXF::HEADER;
use File::DXF::BLOCKS;

use lib ('lib', '../lib');
use Molior::DXF;

my $wall = Molior::DXF->new;
$wall->Read_Style();
$wall->Read_Db();

$wall->{path} = [[0,1], [5,3], [10, 19], [8, 18], [1,5]];
$wall->{outer} = 0.25;
$wall->{inner} = 0.2;
$wall->{elevation} = 3.2;
$wall->{height} = 0.0;

my $opening;
$opening->{name} = 'bedroom outside door';
$opening->{along} = 0.0;
$opening->{up} = 0.0;
$opening->{along} = 1.0;
$opening->{up} = 0.5;
$opening->{size} = 0;

$wall->Add_Opening (0, $opening);

$opening->{type} = 'kitchen inside door';
$opening->{along} = 0.0;
$opening->{up} = 0.0;
$opening->{along} = 2.5;
$opening->{up} = 0.8;
$opening->{size} = 0;

$wall->Add_Opening (0, $opening);

$opening->{type} = 'bedroom outside window';
$opening->{along} = 0.0;
$opening->{up} = 0.0;
$opening->{along} = 2.5;
$opening->{up} = 0.5;
$opening->{size} = 0;

$wall->Add_Opening (1, $opening);

my $dxf = File::DXF->new;
$dxf->{HEADER} = File::DXF::HEADER->new;
$dxf->{ENTITIES} = File::DXF::ENTITIES->new;
$dxf->{BLOCKS} = File::DXF::BLOCKS->new;

push @{$dxf->{ENTITIES}}, $wall->Walls;
$dxf->{BLOCKS}->Add_From_Disk ($wall->Openings_in_use);

open my $DXF, '>', 'bar.dxf' or croak "$!";
print $DXF $dxf->DXF;
close $DXF;
ok (-e 'bar.dxf', 'DXF file exists');
unlink 'bar.dxf';

$wall->Write ('foo.molior');
ok (-e 'foo.molior', 'molior file exists');

my $new = Molior::DXF->new;
$new->Read('foo.molior');
is_deeply($wall->Serialise, $new->Serialise, 'write/read sanity');

my $new2 = Molior::DXF->new;
$new2->Deserialise($new->Serialise);
is_deeply($new->Serialise, $new2->Serialise, 'serialise/deserialise sanity');
is_deeply($new, $new2, 'serialise/deserialise sanity');

unlink 'foo.molior';

0;
