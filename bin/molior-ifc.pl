#!/usr/bin/perl

use strict;
use warnings;
use Carp;
use File::Spec;
use 5.010;

use lib 'lib';
use File::IFC qw /guid real/;
use File::IFC::Helpers;
use Molior::IFC;

croak "usage: $0 wall.molior floor.molior [...]" unless scalar @ARGV;

my $ifc = File::IFC->new;
$ifc->Headers('IFC4');
my $ownerhistory = undef;

my ($body_context, $axis_context, $footprint_context, $annotation_context) = $ifc->GeoContext(['0.','0.','0.']);

my $axis = $body_context->{data}->[6]->{data}->[4];

my $unit_angle   = $ifc->IFCSIUNIT ('*', '.PLANEANGLEUNIT.', undef, '.RADIAN.');
my $unit_mass    = $ifc->IFCSIUNIT ('*', '.MASSUNIT.', '.KILO.', '.GRAM.');
my $unit_force   = $ifc->IFCSIUNIT ('*', '.FORCEUNIT.', '.KILO.', '.NEWTON.');
my $unit_length  = $ifc->IFCSIUNIT ('*', '.LENGTHUNIT.', undef, '.METRE.');
my $unit_area    = $ifc->IFCSIUNIT ('*', '.AREAUNIT.', undef, '.SQUARE_METRE.');
my $unit_volume  = $ifc->IFCSIUNIT ('*', '.VOLUMEUNIT.', undef, '.CUBIC_METRE.');
my $unit_assign  = $ifc->IFCUNITASSIGNMENT ([$unit_angle, $unit_mass, $unit_force, $unit_length, $unit_area, $unit_volume]);

my $project      = $ifc->IFCPROJECT (guid(), $ownerhistory, 'Molior Project', '', undef, '', '', [$body_context->{data}->[6], $axis_context->{data}->[6]], $unit_assign);

my $sites = {};
my $buildings = {};
my $storeys = {};
my $spaces = {};
my $l_usages = {};
my $bounds = {};
my $types = {};

for my $path_molior (@ARGV)
{
    say STDERR $path_molior;
    next if $path_molior =~ /\.ifc$/xi;
    my $molior = Molior::IFC->new;

    for my $item (YAML::LoadFile($path_molior))
    {
    $molior->Deserialise($item);

    croak "Error, component folder not found, please set SHARE_DIR=/path/to/share"
        unless $molior->Share_dir;

    $molior->Read_Style();

    next if (defined $molior->{_style}->{disable}->{$molior->{name}});

    my $building = undef;
    my $parent = undef;
    if (defined $molior->{guid})
    {
        unless (defined $buildings->{$molior->{guid}})
        {
            # define a site and put it in the project
            my @path = map {$ifc->IFCCARTESIANPOINT ($_)} @{$molior->{path}}, $molior->{path}->[0];
            my $centreline  = $ifc->IFCPOLYLINE ([@path]);
            my $representation = $ifc->IFCSHAPEREPRESENTATION ($annotation_context, 'Annotation', 'Curve2D', [$centreline]);
            my $product_representation = $ifc->IFCPRODUCTDEFINITIONSHAPE(undef, undef, [$representation]);

            # FIXME placement should be at z=elevation
            my $placement_site  = $ifc->IFCLOCALPLACEMENT (undef, $axis);
            my $site            = $ifc->IFCSITE (guid(), $ownerhistory, 'My Site', '', undef, $placement_site,
                                      $product_representation, undef, '.ELEMENT.', undef, undef, undef, undef, undef);
                                  $ifc->IFCRELAGGREGATES (guid(), $ownerhistory, '', '', $project, [$site]);
            $sites->{$molior->{guid}} = $site;

            # define a building and put it in the site
            my $placement    = $ifc->IFCLOCALPLACEMENT (undef, $axis);
            my $building_t   = $ifc->IFCBUILDING (guid($molior->{guid}), $ownerhistory, 'Building '.$molior->{guid},
                                   '', undef, $placement, undef, undef, '.ELEMENT.', undef, undef, undef);
                               $ifc->IFCRELAGGREGATES (guid(), $ownerhistory, '', '', $site, [$building_t]);
            $buildings->{$molior->{guid}} = $building_t;
        }
        $building = $buildings->{$molior->{guid}};
    }

    if (defined $molior->{level})
    {
        unless (defined $storeys->{$molior->{guid}}->[$molior->{level}])
        {
            # define a storey and put it in the building
            my $origin_t = $ifc->IFCCARTESIANPOINT (['0.','0.', $molior->{elevation}]);
            my $rotationaxis = $ifc->IFCDIRECTION(['0.','0.','1.']);
            my $rotationdirection = $ifc->IFCDIRECTION(['1.','0.','0.']);

            my $axis_t   = $ifc->IFCAXIS2PLACEMENT3D ($origin_t, $rotationaxis, $rotationdirection);
            my $placement = $ifc->IFCLOCALPLACEMENT (undef, $axis_t);
            my $storey = $ifc->IFCBUILDINGSTOREY (guid($molior->{guid}.$molior->{level}), $ownerhistory, $molior->{plot} .' '. $molior->{level}, '', undef, $placement,
                                             undef, undef, '.ELEMENT.', real($molior->{elevation}));
                         $ifc->IFCRELAGGREGATES (guid(), $ownerhistory, '', '', $building, [$storey]);
            $storeys->{$molior->{guid}}->[$molior->{level}] = $storey;
        }
        $parent = $storeys->{$molior->{guid}}->[$molior->{level}];
    }

    if ($molior->Type eq 'molior-wall')
    {
        $molior->Read_Db();

        my $key = $molior->{name}.'_'.$molior->{inner}.'_'.$molior->{outer};
        unless (defined $l_usages->{$key})
        {
            my $material      = $ifc->IFCMATERIAL ('Masonry', undef, undef);
            my $materiallayer = $ifc->IFCMATERIALLAYER ($material, $molior->{inner} + $molior->{outer}, '.F.', undef, undef, undef, undef);
            my $wallset       = $ifc->IFCMATERIALLAYERSET ([$materiallayer], $key, undef);
            $l_usages->{$key} = $ifc->IFCMATERIALLAYERSETUSAGE ($wallset, '.AXIS2.', '.POSITIVE.', real(0 - $molior->{outer}), undef);
            $types->{$key}    = $ifc->IFCWALLTYPE (guid(), $ownerhistory, $key, undef, undef, undef, undef, undef, undef, '.SOLIDWALL.');
                                $ifc->IFCRELASSOCIATESMATERIAL (guid(), $ownerhistory, undef, undef, [$types->{$key}], $wallset);
        }

        for (0 .. $molior->Segments -1)
        {
            $molior->Fix_Heights ($_) || croak "Can't fix segment $_";
            $molior->Fix_Segment ($_) || croak "Can't fix segment $_";
        }

        my $segments = $molior->Walls (ifc => $ifc, ownerhistory => $ownerhistory, axis => $axis, body_context => $body_context, axis_context => $axis_context, storey => $parent, layer_usage => $l_usages->{$key}, type => $types->{$key});
        $molior->Inserts (ifc => $ifc, ownerhistory => $ownerhistory, axis => $axis, body_context => $body_context, storey => $parent, segments => $segments);

        my $wall_surfaces = $molior->Wall_Surfaces(ifc => $ifc, axis => $axis, body_context => $body_context);

        for (0 .. $molior->Segments -1)
        {
            next unless $molior->{bounds}->[$_];
            # name: party interior exterior fence
            push @{$bounds->{$molior->{guid}}->[$molior->{level}]->{$molior->{bounds}->[$_]->[0]}->{$molior->{name}}}, [$segments->[$_],$wall_surfaces->[$_]->[0]];
            push @{$bounds->{$molior->{guid}}->[$molior->{level}]->{$molior->{bounds}->[$_]->[1]}->{$molior->{name}}}, [$segments->[$_],$wall_surfaces->[$_]->[1]];
        }
    }

    elsif ($molior->Type eq 'molior-roof')
    {
        $molior->Roof (ifc => $ifc, ownerhistory => $ownerhistory, body_context => $body_context, storey => $parent, building => $building);
    }
    elsif ($molior->Type eq 'molior-extrusion')
    {
        $molior->Extrusion (ifc => $ifc, ownerhistory => $ownerhistory, axis => $axis, body_context => $body_context, axis_context => $axis_context, storey => $parent, types => $types);
    }
    # elsif ($molior->Type eq 'molior-intermittent')
    # {
    #     push @{$dxf->{ENTITIES}}, $molior->Intermittent;
    # }
    elsif ($molior->Type eq 'molior-ceiling')
    {
        $molior->Ceiling (ifc => $ifc, ownerhistory => $ownerhistory, axis => $axis, body_context => $body_context, storey => $parent);
    }
    elsif ($molior->Type eq 'molior-floor')
    {
        $molior->Floor (ifc => $ifc, ownerhistory => $ownerhistory, axis => $axis, body_context => $body_context, storey => $parent);
    }
    elsif ($molior->Type eq 'molior-insert')
    {
        $molior->Read_Inserts();
        $molior->Insert_insert (ifc => $ifc, ownerhistory => $ownerhistory, body_context => $body_context, storey => $parent);
    }
    elsif ($molior->Type eq 'molior-stair')
    {
        $molior->Stair (ifc => $ifc, ownerhistory => $ownerhistory, axis => $axis, body_context => $body_context, storey => $parent);
    }
    elsif ($molior->Type eq 'molior-space')
    {
        $spaces->{$molior->{guid}}->[$molior->{level}]->{$molior->{id}} =
        $molior->Space (ifc => $ifc, ownerhistory => $ownerhistory, axis => $axis, body_context => $body_context, storey => $parent);
    }
    elsif ($molior->Type eq 'molior-plot')
    {
        $sites->{$molior->{guid}}->{data}->[2] = 'Site '.$molior->{name};
        $buildings->{$molior->{guid}}->{data}->[2] = 'Building '.$molior->{name};
    }
}
}

for my $building_guid (keys %{$spaces})
{
    my $building = $spaces->{$building_guid};
    for my $level_id (0 .. scalar @{$building} -1)
    {
        my $level = $building->[$level_id];
        for my $space_id (keys %{$level})
        {
            my $space = $level->{$space_id};
            for my $name (keys %{$bounds->{$building_guid}->[$level_id]->{$space_id}})
            {
                for my $wall (@{$bounds->{$building_guid}->[$level_id]->{$space_id}->{$name}})
                {
                    # name: party interior exterior fence
                    my $enum;
                    $enum = '.INTERNAL.' if ($name eq 'interior');
                    $enum = '.EXTERNAL.' if ($name eq 'exterior');
                    $enum = '.EXTERNAL.' if ($name eq 'fence');
                    $enum = '.EXTERNAL.' if ($name eq 'party');
                    my $surfacegeometry = $ifc->IFCCONNECTIONSURFACEGEOMETRY($wall->[1], undef);
                    $ifc->IFCRELSPACEBOUNDARY2NDLEVEL(guid(), $ownerhistory, undef, "$level_id/$space_id", $space, $wall->[0], $surfacegeometry, '.PHYSICAL.', $enum, undef, undef);
                }
            }
        }
    }
}

$ifc->Merge_Ifcrel();

my $path_ifc = 'molior.ifc';
$path_ifc = $ARGV[-1] if $ARGV[-1] =~ /\.ifc$/xi;

open my $IFC, '>', $path_ifc or croak "$!";
binmode $IFC, ":crlf";
print $IFC $ifc->Text;
close $IFC;

0;

__END__

=head1 NAME

molior-ifc - Generate 3D building models

=head1 SYNOPSIS

Create an IFC project from Molior YAML definitions:

  molior.pl thing.molior otherthing.molior [...] things.ifc

=head1 DESCRIPTION

B<molior-ifc> takes a geometry definition from a set of YAML files and writes an IFC file
representation.

=head1 LICENSE

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

=head1 SEE ALSO

L<Urb> L<File::IFC> L<Molior> L<Molior::IFC>

=head1 AUTHOR

Bruno Postle - 2014.

=cut
