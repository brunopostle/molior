#!/usr/bin/perl

use strict;
use warnings;
use Carp;
use File::Spec;
use File::DXF;
use File::DXF::ENTITIES;
use File::DXF::HEADER;
use File::DXF::BLOCKS;
use 5.010;

use lib 'lib';
use Molior::DXF;

croak "usage: $0 wall.molior [roof.molior ... ] [wall.dxf]" unless scalar @ARGV;

my $molior = Molior::DXF->new;

my $dxf = File::DXF->new;
$dxf->{HEADER} = File::DXF::HEADER->new;
$dxf->{ENTITIES} = File::DXF::ENTITIES->new;
$dxf->{BLOCKS} = File::DXF::BLOCKS->new;

for my $path_molior (@ARGV)
{
say STDERR $path_molior;
next if $path_molior =~ /\.dxf$/xi;

for my $item (YAML::LoadFile($path_molior))
{
$molior->Deserialise($item);

croak "Error, component folder not found, please set SHARE_DIR=/path/to/share"
    unless $molior->Share_dir;

$molior->Read_Style();

if (defined $molior->{_style}->{disable}->{$molior->{name}})
{
    # do nothing
}
elsif ($molior->Type eq 'molior-wall')
{
    $molior->Read_Db();
    for (0 .. $molior->Segments -1)
    {
        $molior->Fix_Heights ($_) || croak "Can't fix heights $_";
        $molior->Fix_Segment ($_) || croak "Can't fix segment $_";
    }

    push @{$dxf->{ENTITIES}}, $molior->Walls;
    for my $path_dxf ($molior->Openings_in_use)
    {
        my ($v, $d, $name) = File::Spec->splitpath ($path_dxf);
        $name =~ s/\.[[:alnum:]]+$//x;
        $dxf->{BLOCKS}->Add_From_Disk ($path_dxf) unless defined $dxf->{BLOCKS}->{$name};
    }
}
elsif ($molior->Type eq 'molior-roof')
{
    push @{$dxf->{ENTITIES}}, $molior->Roof;
}
elsif ($molior->Type eq 'molior-extrusion')
{
    push @{$dxf->{ENTITIES}}, $molior->Extrusion;
}
elsif ($molior->Type eq 'molior-intermittent')
{
    push @{$dxf->{ENTITIES}}, $molior->Intermittent;
}
elsif ($molior->Type eq 'molior-ceiling')
{
    push @{$dxf->{ENTITIES}}, $molior->Ceiling;
}
elsif ($molior->Type eq 'molior-floor')
{
    push @{$dxf->{ENTITIES}}, $molior->Floor;
}
elsif ($molior->Type eq 'molior-insert')
{
    $molior->Read_Inserts();
    push @{$dxf->{ENTITIES}}, $molior->Insert_insert;
    my $path_dxf = $molior->Insert_in_use;
    my ($v, $d, $name) = File::Spec->splitpath ($path_dxf);
    $name =~ s/\.[[:alnum:]]+$//x;
    $dxf->{BLOCKS}->Add_From_Disk ($path_dxf) unless defined $dxf->{BLOCKS}->{$name};
}
elsif ($molior->Type eq 'molior-stair')
{
    push @{$dxf->{ENTITIES}}, $molior->Stair;
}
elsif ($molior->Type eq 'molior-space')
{
    push @{$dxf->{ENTITIES}}, $molior->Light unless ($molior->{usage} eq 'Outside' and $molior->{level} == 0);
}

}
}

my $path_dxf = 'molior.dxf';
$path_dxf = $ARGV[-1] if $ARGV[-1] =~ /\.dxf$/xi;

open my $DXF, '>', $path_dxf or croak "$!";
binmode $DXF, ":crlf";
print $DXF $dxf->DXF;
close $DXF;

0;

__END__

=head1 NAME

molior - Generate 3D building models

=head1 SYNOPSIS

Create a DXF file from Molior YAML definitions:

  molior.pl thing.molior [otherthing.molior ...] things.dxf

Convert the DXF file to a Collada file:

  polyline2collada.pl things.dxf things.dae

=head1 DESCRIPTION

B<molior> takes geometry definitions from YAML files and writes a DXF file
representation.

=head1 LICENSE

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

=head1 SEE ALSO

L<Urb> L<File::DXF> L<Molior> L<Molior::DXF>

=head1 AUTHOR

Bruno Postle - 2011.

=cut
