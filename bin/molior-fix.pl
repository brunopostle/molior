#!/usr/bin/perl

use strict;
use warnings;
use Carp;
use File::Spec;

use lib 'lib';
use Molior;

croak "usage: $0 wall.molior wall-fixed.molior" unless scalar @ARGV == 2;

my $molior = Molior->new;
$molior->Read ($ARGV[0]) || croak "Can't open $ARGV[0]";

croak "Error, component folder not found, please set SHARE_DIR=/path/to/share"
    unless $molior->Share_dir;

if ($molior->Type eq 'molior-wall')
{
    $molior->Read_Db();
    $molior->Read_Style();

    for (0 .. $molior->Segments -1)
    {
        $molior->Fix_Heights ($_) || croak "Can't fix segment $_";
        $molior->Fix_Segment ($_) || croak "Can't fix segment $_";
    }
}

$molior->Write ($ARGV[1]);

0;

__END__

=head1 NAME

molior-fix - Fix molior wall definitions

=head1 SYNOPSIS

Shuffle (and remove) openings in a Molior wall YAML definition:

  molior-fix.pl wall.molior wall-fixed.molior

=head1 DESCRIPTION

B<molior-fix> takes a geometry definition from a YAML file and moves openings
such that they don't clash.  If this can't be achieved then openings are deleted
from the right hand side of each segment until there is no clash.

Also lowers openings that don't fit within the height of the wall.

=head1 LICENSE

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

=head1 SEE ALSO

L<Urb>, L<File::DXF>, L<Molior>, L<molior.pl>

=head1 AUTHOR

Bruno Postle - 2013.

=cut
