#!/usr/bin/perl

use strict;
use warnings;
use Carp;
use File::Copy;

use lib 'lib';
use Molior;

croak "usage: $0 thing1.molior thing2.molior [...]" unless scalar @ARGV;

my $index = 0;

for my $path (@ARGV)
{
    my $molior = Molior->new;
    $molior->Read ($path) || croak "Can't open $path";

    mkdir $molior->Type;

    copy ($path, $molior->Type ."/$index.molior");
    $index++;
}

0;

