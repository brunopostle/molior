package Molior;

use strict;
use warnings;
use lib ('lib', '../lib');
use YAML;
use File::Spec;
use Math::Trig;
use File::ShareDir;
use File::DXF::Math qw /rotate_3d distance_2d angle_2d subtract_2d add_2d scale_2d normalise_2d points_2line line_intersection is_between_2d/;
use 5.010;

our $VERSION = 0.01;
our $PI = atan2 (0,-1);

our $SHARE_DIR = File::ShareDir::module_dir('Molior');
$SHARE_DIR = $ENV{SHARE_DIR} if defined $ENV{SHARE_DIR} and -d $ENV{SHARE_DIR};

=head1 NAME

Molior - Generate 3D building models

=head1 SYNOPSIS

  use Molior;

  my $molior = Molior->new ;
  $molior->Read ('extrusion.molior');

=head1 DESCRIPTION

Takes simplified descriptions of building parts stored in YAML files and
derives 3D geometry.

Note this is a base class, use L<Molior::DXF> or L<Molior::IFC> for creating output.

=head1 USAGE

=over

=item new

Create a Molior instance:

  use Molior;
  my $molior = Molior->new ;

=cut

sub new
{
    my ($class, $attributes) = @_;
    $class = ref $class || $class;

    my $self = {};
    $self = $attributes if defined $attributes;

    bless $self, $class;
    return $self;
}

=item Read

Read a YAML definition from disk:

  $molior->Read ('/path/to/thing.molior') || die;

=cut

sub Read
{
    my $self = shift;
    my $path = shift || return 0;
    return 0 unless -e $path;
    my $hash = YAML::LoadFile ($path);
    $self->Deserialise ($hash);
}

sub Deserialise
{
    my ($self, $hash) = @_;
    return 0 unless defined $hash and ref $hash eq 'HASH';
    delete $self->{$_} for keys %{$self};
    for my $key (keys %{$hash})
    {
        if ($hash->{$key} and $hash->{$key} eq 'null')
        {
            $self->{$key} = undef;
        }
        else
        {
            $self->{$key} = $hash->{$key};
        }
    }
    return 1;
}

=item Write

Write a YAML definition to disk:

  $molior->Write ('/path/to/thing.molior') || die;

=cut

sub Write
{
    my $self = shift;
    my $path = shift || return 0;
    my $hash = $self->Serialise;
    YAML::DumpFile ($path, $hash) || return 0;
    return 1;
}

sub Serialise
{
    my $self = shift;
    my $hash = {};
    for my $key (keys %{$self})
    {
        next if $key =~ /^_/x;
        $hash->{$key} = $self->{$key};
    }
    return $hash;
}

=item Share_dir

Check if the share directory for part/component database exists:

  my $share_dire = $molior->Share_dir
      || die 'Can't find component folder set SHARE_DIR=/path/to/share';

The path can be set using $ENV{SHARE_DIR} or using the Share_dir() method:

..in bash:

  $ export SHARE_DIR=/usr/local/share/molior

..or in perl:

  $molior->Share_dir ('/usr/share/molior');

=cut

sub Share_dir
{
    my ($self, @list) = @_;
    $SHARE_DIR = shift @list if scalar @list;
    if (defined $self->{style} and $self->{style} =~ /[[:alnum:] ()._-]+/)
    {
        my $style_dir = File::Spec->catdir ($SHARE_DIR, $self->{style});
        return $style_dir if -d $style_dir;
        say STDERR 'Warning: I got no style \''.$self->{style}.'\', using default style';
    }
    return $SHARE_DIR if -d $SHARE_DIR;
    return;
}

=item Read_Db Read_Style

Slurp up a list of pre-defined openings from a YAML file, add to $self->{_db} and $self->{_style} hash

  $molior->Read_Db();
  $molior->Read_Style();

=cut

sub Read_Db
{
    my $self = shift;

    my $path = File::Spec->catfile ($SHARE_DIR, 'openings.yml');
    $self->{_db} = YAML::LoadFile ($path);
    return 1 if $SHARE_DIR eq $self->Share_dir;

    $path = File::Spec->catfile ($self->Share_dir, 'openings.yml');
    if (!-e $path)
    {
        say STDERR 'Warning: using default for openings.yml';
        return 1;
    }

    my $overlay_db = YAML::LoadFile ($path);
    for my $name (keys %{$overlay_db})
    {
        $self->{_db}->{$name} = $overlay_db->{$name};
    }
    return 1;
}

sub Read_Inserts
{
    my $self = shift;

    my $path = File::Spec->catfile ($SHARE_DIR, 'inserts.yml');
    $self->{_insert} = YAML::LoadFile ($path);
    return 1 if $SHARE_DIR eq $self->Share_dir;

    $path = File::Spec->catfile ($self->Share_dir, 'inserts.yml');
    if (!-e $path)
    {
        say STDERR 'Warning: using default for inserts.yml';
        return 1;
    }

    my $overlay_inserts = YAML::LoadFile ($path);
    for my $name (keys %{$overlay_inserts})
    {
        $self->{_insert}->{$name} = $overlay_inserts->{$name};
    }
    return 1;
}

sub Read_Style
{
    my $self = shift;

    my $path = File::Spec->catfile ($SHARE_DIR, 'style.yml');
    $self->{_style} = YAML::LoadFile ($path);
    return 1 if $SHARE_DIR eq $self->Share_dir;

    $path = File::Spec->catfile ($self->Share_dir, 'style.yml');
    if (!-e $path)
    {
        say STDERR 'Warning: using default for style.yml';
        return 1;
    }

    my $overlay = YAML::LoadFile ($path);

    $self->{_style}->{roof_gradient} = $overlay->{roof_gradient} if defined $overlay->{roof_gradient};
    $self->{_style}->{disable} = $overlay->{disable} if defined $overlay->{disable};

    for my $type (keys %{$overlay->{colour}})
    {
        for my $name (keys %{$overlay->{colour}->{$type}})
        {
            $self->{_style}->{colour}->{$type}->{$name} = $overlay->{colour}->{$type}->{$name};
        }
    }

    for my $name (keys %{$overlay->{profile}})
    {
        $self->{_style}->{profile}->{$name} = $overlay->{profile}->{$name};
    }

    for my $name (keys %{$overlay->{opening}})
    {
        $self->{_style}->{opening}->{$name} = $overlay->{opening}->{$name};
    }

    return 1;
}

=item Add_Opening Get_Opening

Place an opening object into a segment by id:

  $molior->Add_Opening (0, $opening);

Retrieve a list of possible opening objects, a type and a miniumum cill height given an opening usage:

  $hashref = $molior->Get_Opening ('kitchen outside window');
  say 'Cill height: '.$hashref->{cill};
  say 'Number of windows in this set: '.scalar @{$hashref->{list}};
  say 'Window or Door?: '.$hashref->{type};

=cut

sub Add_Opening
{
    my ($self, $id_segment, $opening) = @_;
    push @{$self->{openings}->[$id_segment]}, $opening;
    return;
}

sub Get_Opening
{
    my ($self, $usage) = @_;
    unless (defined $self->{_style}->{opening}->{$usage})
    {
        say STDERR "warning: opening for $usage not found";
        return {list => [{file => 'error.dxf', height => 1.0, width => 1.0, side => 0.1, end => 0.0}],
               type => 'door', cill => 1.0};
    }

    my $style = $self->{_style}->{opening}->{$usage};
    my $db = $self->{_db}->{$style->{name}};
    return {list => $db, type => $style->{type}, cill => $style->{cill}};
}

sub Get_Insert
{
    my ($self, $usage) = @_;
    unless (defined $self->{_style}->{insert}->{$usage})
    {
        say STDERR "warning: insert for $usage not found";
        return {list => [{file => 'error.dxf', bounds => [[-0.5,-0.1,0.0],[0.5,0.1,1.0]]}],
               type => 'error'};
    }

    my $style = $self->{_style}->{insert}->{$usage};
    my $list = $self->{_insert}->{$style->{name}};
    return {list => $list, type => $style->{type}};
}

sub Insert_in_use
{
    my $self = shift;
    return unless $self->Type eq 'molior-insert';
    $self->{size} = 0 unless defined $self->{size};
    my $db = $self->Get_Insert ($self->{name});

    # the molior file specifies the bounding-box that the insert needs to fit
    # find the biggest insert that will fit in this space (otherwise the smallest)
    my $space = $self->{space} || [[-1,-1,0],[1,1,5]];
    my $best = {};
    for my $id (0 .. scalar @{$db->{list}} -1)
    {
        my $try = $db->{list}->[$id]->{bounds};
        my $excess = -100.0;
        my $under = 100.0;

        my $left = 0 - ($try->[0]->[0] - $space->[0]->[0]);
        $excess = $left if $left > $excess;
        $under = $left if $left < $under;
        my $right = $try->[1]->[0] - $space->[1]->[0];
        $excess = $right if $right > $excess;
        $under = $right if $right < $under;

        my $front = 0 - ($try->[0]->[1] - $space->[0]->[1]);
        $excess = $front if $front > $excess;
        $under = $front if $front < $under;
        my $back = $try->[1]->[1] - $space->[1]->[1];
        $excess = $back if $back > $excess;
        $under = $back if $back < $under;

        my $bottom = 0 - ($try->[0]->[2] - $space->[0]->[2]);
        $excess = $bottom if $bottom > $excess;
        $under = $bottom if $bottom < $under;
        my $top = $try->[1]->[2] - $space->[1]->[2];
        $excess = $top if $top > $excess;
        $under = $top if $top < $under;

        $excess = $under if $excess <= 0.0;
        $best->{$excess} = $id;
    }

    my $error = 0.0;
    for (sort {$b <=> $a} keys %{$best})
    {
        $self->{size} = $best->{$_};
        $error = $_;
        last if $_ <= 0.0;
    }

    my $filename = $db->{list}->[$self->{size}]->{file};
    say STDERR "Warning: $filename overfull hbox" if $error > 0.0;
    my $path_dxf = File::Spec->catfile ($self->Share_dir, $filename);
    if (!-e $path_dxf)
    {
        $path_dxf = File::Spec->catfile ($SHARE_DIR, $filename);
        say STDERR 'Warning: using default for '.$filename;
    }
    return $path_dxf;
}

=item Fix_Segment Fix_Heights Fix_Overlaps Fix_Overrun Fix_Underrun

Shuffle openings and delete the last in a segment if necessary to ensure
there are no overlapping openings or openings beyond the segment.

  $molior->Fix_Segment (2);

Lower openings if overall height doesn't fit in wall:

  $molior->Fix_Heights (2);

=cut

sub Fix_Heights
{
    my $self = shift;
    my $id_segment = shift;
    my $openings = $self->{openings}->[$id_segment] || return 1;
    return 1 unless ref $openings eq 'ARRAY';

    my $soffit = $self->{height} - $self->{ceiling};

    my $openings_new = [];
    for my $opening (@{$openings})
    {
        my $db = $self->Get_Opening ($opening->{name});
        my $list = $db->{list};
        $opening->{up} = $db->{cill};
        my $width_original = $list->[$opening->{size}]->{width};

        my $heights = {};
        map {$heights->{$_} = $list->[$_]->{height}} (0 .. scalar @{$list} -1);
        my @highest = sort {$heights->{$b} <=> $heights->{$a}} (0 .. scalar @{$list} -1);
        for my $size (@highest)
        {
             next unless $list->[$size]->{width} == $width_original;
             $opening->{size} = $size;

             last if $opening->{up} + $list->[$opening->{size}]->{height} <= $soffit;
        }

        while ($opening->{up} + $list->[$opening->{size}]->{height} >= $soffit and $opening->{up} >= 0.15)
        {
            say STDERR 'Warning: shorter '. $opening->{name} .' not found, fixing height by dropping cill';
            $opening->{up} -= 0.15;
        }
        push(@{$openings_new}, $opening) if $opening->{up} + $list->[$opening->{size}]->{height} <= $soffit;
    }
    $self->{openings}->[$id_segment] = $openings_new;
    return 1;
}

sub Fix_Overlaps
{
    my $self = shift;
    my $id_segment = shift;

    my $openings = $self->{openings}->[$id_segment] || return 1;
    return 1 unless defined $openings->[0];

    # fix overlaps by sliding forward next opening until it is spaced by $border
    for my $id_opening (0 .. scalar @{$openings} -2)
    {
        my $opening = $openings->[$id_opening];
        my $db = $self->Get_Opening ($opening->{name});
        my $width = $db->{list}->[$opening->{size}]->{width};
        my $side  = $db->{list}->[$opening->{size}]->{side};

        my $opening_next = $openings->[$id_opening +1];
        my $db_next = $self->Get_Opening ($opening_next->{name});
        my $side_next = $db_next->{list}->[$opening_next->{size}]->{side};

        my $border = $side;
        $border = $side_next if $side_next > $border;

        my $along_ok = $opening->{along} + $width + $border;
        next if $along_ok <= $openings->[$id_opening +1]->{along};
        $openings->[$id_opening +1]->{along} = $along_ok;
    }
    return 1;
}

sub Fix_Overrun
{
    my $self = shift;
    my $id_segment = shift;

    my $openings = $self->{openings}->[$id_segment] || return 1;
    return 1 unless defined $openings->[0];
    # this is the space we can use to fit windows and doors
    my $length = $self->Length_Segment ($id_segment)
               - $self->Border($id_segment)->[1];

    # fix overrun by sliding all openings backward by amount of overrun
    my $db_last = $self->Get_Opening ($openings->[-1]->{name});
    my $width_last = $db_last->{list}->[$openings->[-1]->{size}]->{width};
    my $end_last   = $db_last->{list}->[$openings->[-1]->{size}]->{end};

    my $overrun = $openings->[-1]->{along} + $width_last + $end_last - $length;
    return 1 if $overrun <= 0;

    for my $id_opening (0 .. scalar @{$openings} -1)
    {
        $openings->[$id_opening]->{along} -= $overrun;
    }
    return 1;
}

sub Fix_Underrun
{
    my $self = shift;
    my $id_segment = shift;

    my $openings = $self->{openings}->[$id_segment] || return 1;
    return 1 unless defined $openings->[0];

    # fix underrun by sliding all but last opening forward by no more than amount of underrun or until spaced by border
    my $db_first = $self->Get_Opening ($openings->[0]->{name});
    my $end_first = $db_first->{list}->[$openings->[0]->{size}]->{end};

    my $underrun = $self->Border($id_segment)->[0] + $end_first - $openings->[0]->{along};
    return 1 if $underrun <= 0;

    for my $id_opening (reverse (0 .. scalar @{$openings} -2))
    {
        my $opening = $openings->[$id_opening];
        my $db = $self->Get_Opening ($opening->{name});
        my $width = $db->{list}->[$opening->{size}]->{width};
        my $side = $db->{list}->[$opening->{size}]->{side};

        my $opening_next = $openings->[$id_opening +1];
        my $db_next = $self->Get_Opening ($opening_next->{name});
        my $side_next = $db_next->{list}->[$opening_next->{size}]->{side};

        my $border = $side;
        $border = $side_next if $side_next > $border;

        my $along_ok = $openings->[$id_opening +1]->{along} - $border - $width;
        next if $opening->{along} >= $along_ok;
        my $slide = $along_ok - $opening->{along};
        $slide = $underrun if $slide > $underrun;
        $opening->{along} += $slide;
    }
    return 1;
}

sub Border
{
    my $self = shift;
    my $id_segment = shift;

    # walls are drawn anti-clockwise around the building,
    # the distance 'along' is from left-to-right as looking from outside
    my $border_left = 0.0;
    my $border_right = 0.0;

    if (!$self->{closed} and $id_segment == 0)
    {
        $border_left = $self->{inner};
    }
    else
    {
        my $angle_left = $self->Angle_Segment ($id_segment) - $self->Angle_Segment ($id_segment -1);
        $angle_left += 360.0 if $angle_left < 0.0;
        $border_left = $self->{inner};
        $border_left = $self->{outer} if ($angle_left > 135.0 and $angle_left < 315.0);
    }

    if (!$self->{closed} and $id_segment == scalar (@{$self->{path}} -2))
    {
        $border_right = $self->{inner};
    }
    else
    {
        my $segment_right = $id_segment +1;
        $segment_right = 0 if $segment_right == scalar (@{$self->{path}});
        my $angle_right = $self->Angle_Segment ($segment_right) - $self->Angle_Segment ($id_segment);
        $angle_right += 360.0 if $angle_right < 0.0;
        $border_right = $self->{inner};
        $border_right = $self->{outer} if ($angle_right > 135.0 and $angle_right < 315.0);
    }
    return [$border_left, $border_right];
}

sub Length_Openings
{
    my $self = shift;
    my $id_segment = shift;

    my $openings = $self->{openings}->[$id_segment] || return 0.0;
    return 0.0 unless defined $openings->[0];

    my $db_first = $self->Get_Opening ($openings->[0]->{name});
    my $end_first = $db_first->{list}->[$openings->[0]->{size}]->{end};
    my $db_last = $self->Get_Opening ($openings->[-1]->{name});
    my $end_last = $db_last->{list}->[$openings->[-1]->{size}]->{end};

    # start with end spacing
    my $length_openings = $end_first + $end_last;
    # add width of all the openings
    for my $id_opening (0 .. scalar (@{$openings}) -1)
    {
        my $db = $self->Get_Opening ($openings->[$id_opening]->{name});
        my $width = $db->{list}->[$openings->[$id_opening]->{size}]->{width};
        $length_openings += $width;
    }
    # add wall between openings
    for my $id_opening (0 .. scalar (@{$openings}) -2)
    {
        next unless scalar (@{$openings}) > 1;
        my $db = $self->Get_Opening ($openings->[$id_opening]->{name});
        my $side = $db->{list}->[$openings->[$id_opening]->{size}]->{side};

        my $db_next = $self->Get_Opening ($openings->[$id_opening +1]->{name});
        my $side_next = $db_next->{list}->[$openings->[$id_opening +1]->{size}]->{side};

        $side = $side_next if $side_next > $side;
        $length_openings += $side;
    }
    return $length_openings;
}

sub Fix_Segment
{
    my $self = shift;
    my $id_segment = shift;

    my $openings = $self->{openings}->[$id_segment] || return 1;
    return 1 unless ref $openings eq 'ARRAY';

    # this is the space we can use to fit windows and doors
    my $length = $self->Length_Segment ($id_segment)
               - $self->Border($id_segment)->[0] - $self->Border($id_segment)->[1];

    # reduce multiple windows to one, multiple doors to one
    my ($found_door, $found_window);
    my $new_openings = [];
    for my $opening (@{$openings})
    {
        my $db = $self->Get_Opening ($opening->{name});
        my $type = $db->{type};
        if ($type eq 'door' and !$found_door)
        {
            push @{$new_openings}, $opening;
            $found_door = 1;
        }
        if ($type eq 'window' and !$found_window)
        {
            # set any window to narrowest of this height
            my $list = $db->{list};
            my $height_original = $list->[$opening->{size}]->{height};

            my $widths = {};
            map {$widths->{$_} = $list->[$_]->{width}} (0 .. scalar @{$list} -1);
            my @widest = sort {$widths->{$b} <=> $widths->{$a}} (0 .. scalar @{$list} -1);
            for my $size (@widest)
            {
                 next unless $list->[$size]->{height} == $height_original;
                 $opening->{size} = $size;
            }

            push @{$new_openings}, $opening;
            $found_window = 1;
        }
    }
    $self->{openings}->[$id_segment] = $new_openings;
    $openings = $new_openings;

    # find a door, fit the widest of this height
    for my $opening (@{$openings})
    {
        my $db = $self->Get_Opening ($opening->{name});
        if ($db->{type} eq 'door')
        {
            my $list = $db->{list};
            my $height_original = $list->[$opening->{size}]->{height};

            my $widths = {};
            map {$widths->{$_} = $list->[$_]->{width}} (0 .. scalar @{$list} -1);
            my @widest = sort {$widths->{$b} <=> $widths->{$a}} (0 .. scalar @{$list} -1);
            for my $size (@widest)
            {
                 next unless $list->[$size]->{height} == $height_original;
                 $opening->{size} = $size;

                 # this fits the wisest door for the wall ignoring any window
                 my $end = $list->[$size]->{end};
                 my $width = $list->[$size]->{width};
                 last if $end + $width + $end < $length;

                 # this fits widest door compatible with narrowest window
                 #last if $self->Length_Openings ($id_segment) < $length;
            }
        }
    }

    # find a window, fit the widest of this height
    for my $opening (@{$openings})
    {
        my $db = $self->Get_Opening ($opening->{name});
        if ($db->{type} eq 'window')
        {
            my $list = $db->{list};
            my $height_original = $list->[$opening->{size}]->{height};

            my $widths = {};
            map {$widths->{$_} = $list->[$_]->{width}} (0 .. scalar @{$list} -1);
            my @widest = sort {$widths->{$b} <=> $widths->{$a}} (0 .. scalar @{$list} -1);
            for my $size (@widest)
            {
                next unless $list->[$size]->{height} == $height_original;
                $opening->{size} = $size;

                last if $self->Length_Openings ($id_segment) < $length;
            }

            # duplicate window if possible until filled
            $self->{openings}->[$id_segment] = $openings;
            last if $length < 0.0;
            splice @{$self->{openings}->[$id_segment]}, 1, 0, {%{$opening}}, {%{$opening}};
            if ($self->Length_Openings ($id_segment) > $length)
            {
                splice @{$self->{openings}->[$id_segment]}, 1, 1;
            }
            if ($self->Length_Openings ($id_segment) > $length)
            {
                splice @{$self->{openings}->[$id_segment]}, 1, 1;
            }
        }
    }

    my $length_openings = $self->Length_Openings ($id_segment);

    if ($length_openings > $length)
    {
        if (scalar @{$self->{openings}->[$id_segment]} == 1)
        {
            shift @{$self->{openings}->[$id_segment]};
            #say STDERR 'deleting only opening';
            return 1;
        }

        my $db_last = $self->Get_Opening ($openings->[-1]->{name});

        if ($db_last->{type} eq 'door')
        {
            shift @{$self->{openings}->[$id_segment]};
            #say STDERR 'deleting the first opening and trying again';
        }
        else
        {
            pop @{$self->{openings}->[$id_segment]};
            #say STDERR 'deleting the last opening and trying again';
        }
        $self->Fix_Segment ($id_segment);
        return 1;
    }
 
    #say STDERR "Id: $id_segment, Length: $length, Openings: $length_openings";

    $self->Align_Openings ($id_segment);
    $self->Fix_Overlaps ($id_segment);
    $self->Fix_Overrun ($id_segment);
    $self->Fix_Underrun ($id_segment);

    return 1;
}

sub Align_Openings
{
    my $self = shift;
    my $id_segment = shift;
    return 1 if ($self->{name} eq 'interior');
    my $openings = $self->{openings}->[$id_segment] || return 1;
    return 1 unless defined $openings->[0];
    my $length = $self->Length_Segment ($id_segment);
    my $module = $length / scalar @{$openings};

    my $along = $module/2;
    for my $id_opening (0 .. scalar (@{$openings}) -1)
    {
        my $db = $self->Get_Opening ($openings->[$id_opening]->{name});
        my $width = $db->{list}->[$openings->[$id_opening]->{size}]->{width};
        $openings->[$id_opening]->{along} = $along - ($width/2);
        $along += $module;
    }
    return 1;
}

=item Type

Query or set the 'type' of this molior instance, e.g. 'molior-wall', or
'molior-extrusion':

  my $type = $molior->Type;
  $molior->Type ('molior-extrusion');

=cut

sub Type
{
    my ($self, @type) = @_;
    $self->{type} = shift @type if scalar @type;
    return $self->{type};
}

=item Is_Quadrilateral

Some stuff is only practical with quadrilaterals:

  $ok = $molior->Is_Quadrilateral;

=cut

sub Is_Quadrilateral
{
    my $self = shift;
    return 0 unless scalar @{$self->{path}} == 4;
    return 0 unless $self->{closed};
    my $path = $self->{path};
    return 0 unless distance_2d ($path->[0], $path->[1]);
    return 0 unless distance_2d ($path->[1], $path->[2]);
    return 0 unless distance_2d ($path->[2], $path->[3]);
    return 0 unless distance_2d ($path->[3], $path->[0]);
    return 0 unless distance_2d ($path->[0], $path->[2]);
    return 0 unless distance_2d ($path->[1], $path->[3]);
    return 1;
}

=item Beams

A molior-ceiling object can have 'intermittent' attributes that imply a series of
parallel beams rather than a solid slab, i.e 50m wide beams at 600mm spacing:

  $molior->{intermittent} = [0.6, 0.05];
  @list_of_2d_quads = $molior->Beam;

=cut

sub Beams
{
    my $self = shift;
    return unless defined $self->{intermittent};
    return unless $self->Is_Quadrilateral;
    my $spacing = scale_2d ($self->{intermittent}->[0], $self->Direction_Segment (0));
    my $offset = scale_2d ($self->{intermittent}->[1]/2, $self->Direction_Segment (0));

    my $normal = $self->Normal_Segment (0);

    my $path = $self->{path};
    if ($self->Length_Segment (0) < $self->Length_Segment (1))
    {
        my $newpath = [$path->[1], $path->[2], $path->[3], $path->[0]];
        $path = $newpath;
        $spacing = scale_2d ($self->{intermittent}->[0], $self->Direction_Segment (1));
        $offset = scale_2d ($self->{intermittent}->[1]/2, $self->Direction_Segment (1));
        $normal = $self->Normal_Segment (1);
    }

    my @beams;
    my $inc = -5;

    my $line_0 = points_2line ($path->[0], $path->[1]);
    my $line_1 = points_2line ($path->[1], $path->[2]);
    my $line_2 = points_2line ($path->[2], $path->[3]);
    my $line_3 = points_2line ($path->[3], $path->[0]);

    while ($inc * $self->{intermittent}->[0] < 15)
    {
        my $start = add_2d ($path->[0], scale_2d ($inc, $spacing));
        my $line_beam = points_2line ($start, add_2d ($start, $normal));

        my $intersection_0 = line_intersection ($line_beam, $line_0);
        my $intersection_1 = line_intersection ($line_beam, $line_1);
        my $intersection_2 = line_intersection ($line_beam, $line_2);
        my $intersection_3 = line_intersection ($line_beam, $line_3);

        my $on_0 = is_between_2d ($intersection_0, $path->[0], $path->[1]);
        my $on_1 = is_between_2d ($intersection_1, $path->[1], $path->[2]);
        my $on_2 = is_between_2d ($intersection_2, $path->[2], $path->[3]);
        my $on_3 = is_between_2d ($intersection_3, $path->[3], $path->[0]);

        if ($on_0 and $on_2 and distance_2d($intersection_0, $intersection_2) > 0.01)
        {
            push @beams, [subtract_2d ($intersection_0, $offset), add_2d ($intersection_0, $offset),
                          add_2d ($intersection_2, $offset), subtract_2d ($intersection_2, $offset)];
        }
        elsif ($on_0 and $on_1 and distance_2d($intersection_0, $intersection_1) > 0.01)
        {
            push @beams, [subtract_2d ($intersection_0, $offset), add_2d ($intersection_0, $offset),
                          add_2d ($intersection_1, $offset), subtract_2d ($intersection_1, $offset)];
        }
        elsif ($on_1 and $on_2 and distance_2d($intersection_1, $intersection_2) > 0.01)
        {
            push @beams, [subtract_2d ($intersection_1, $offset), add_2d ($intersection_1, $offset),
                          add_2d ($intersection_2, $offset), subtract_2d ($intersection_2, $offset)];
        }
        elsif ($on_2 and $on_3 and distance_2d($intersection_2, $intersection_3) > 0.01)
        {
            push @beams, [subtract_2d ($intersection_3, $offset), add_2d ($intersection_3, $offset),
                          add_2d ($intersection_2, $offset), subtract_2d ($intersection_2, $offset)];
        }
        elsif ($on_3 and $on_0 and distance_2d($intersection_3, $intersection_0) > 0.01)
        {
            say distance_2d ($intersection_3, $intersection_0);
            push @beams, [subtract_2d ($intersection_0, $offset), add_2d ($intersection_0, $offset),
                          add_2d ($intersection_3, $offset), subtract_2d ($intersection_3, $offset)];
        }

        $inc++;
    }
    return @beams;
}


=item Segments

Query the number of segments in a path:

  my $int = $molior->Segments;

=cut

sub Segments
{
    my $self = shift;
    my $segments = scalar @{$self->{path}};
    $segments -= 1 unless ($self->{closed});
    return $segments;
}

=item Length_Segment Angle_Segment Direction_Segment Normal_Segment

Get the length of a segment:

  $length = $molior->Length_Segment ($id);

Get the angle of a segment (degrees anticlockwise from y=0):

  $degrees = $molior->Angle_Segment ($id);

Get the direction of a segment (as a normalised 2d vector):

  $vec_2d = $molior->Direction_Segment ($id);

Get the normal vector of a segment (2d pointing out):

  $vec_2d = $molior->Normal_Segment ($id);

=cut

sub Length_Segment
{
    my $self = shift;
    my $id_segment = shift;
    my $end_vertex = $id_segment +1;
    $end_vertex = 0 if ($end_vertex == scalar @{$self->{path}});
    return distance_2d ($self->{path}->[$end_vertex], $self->{path}->[$id_segment]);
}

sub Angle_Segment
{
    my $self = shift;
    my $id_segment = shift;
    my $end_vertex = $id_segment +1;
    $end_vertex = 0 if ($end_vertex == scalar @{$self->{path}});
    return angle_2d ($self->{path}->[$id_segment], $self->{path}->[$end_vertex]) * 180/$PI;
}

sub Direction_Segment
{
    my $self = shift;
    my $id_segment = shift;
    my $end_vertex = $id_segment +1;
    $end_vertex -= scalar @{$self->{path}} if ($end_vertex >= scalar @{$self->{path}});
    my $segment_vector = subtract_2d ($self->{path}->[$end_vertex],
                                       $self->{path}->[$id_segment]);
    return normalise_2d ($segment_vector);
}

sub Normal_Segment
{
    my $self = shift;
    my $id_segment = shift;
    $id_segment -= scalar @{$self->{path}} if ($id_segment >= scalar @{$self->{path}});
    my $a = $self->Direction_Segment ($id_segment);
    return [$a->[1], 0-$a->[0]];
}

=item Corner_coor Corner_out Corner_in Corner_offset

Get the 2d coordinates of a corner:

  $coor_2d = $molior->Corner_coor ($id);

Get the 2d coordinates of the outside of a corner:

  $coor_2d = $molior->Corner_out ($id);

Get the 2d coordinates of the inside of a corner:

  $coor_2d = $molior->Corner_in ($id);

Get the 2d coordinates of a corner offset by an arbitrary distance:

  $coor_2d = $molior->Corner_offset ($id, 0.25);

=cut

sub Corner_plane
{
    my $self = shift;
    my $id = shift;
    my $vector = normalise_2d(subtract_2d($self->Corner_offset($id, 1.0), $self->Corner_coor($id)));
    return rotate_3d($PI/2, [@{$vector}, 0.0]);
}

sub Corner_coor
{
    my $self = shift;
    my $id = shift;
    $id -= scalar @{$self->{path}} if ($id >= scalar @{$self->{path}});
    $id += scalar @{$self->{path}} if ($id < 0);
    return $self->{path}->[$id];
}

# locate the offset outside corner

sub Corner_out
{
    my $self = shift;
    my $id = shift;

    return $self->Corner_offset ($id, $self->{outer});
}

sub Corner_in
{
    my $self = shift;
    my $id = shift;
    $self->{inner} = 1.0 unless defined $self->{inner};

    return $self->Corner_offset ($id, 0-$self->{inner});
}

sub Corner_offset
{
    my $self = shift;
    my $id = shift;
    my $distance = shift;

    $id -= scalar @{$self->{path}} if ($id >= scalar @{$self->{path}});

    my $offset_a = scale_2d ($distance, $self->Normal_Segment ($id -1));
    my $offset_b = scale_2d ($distance, $self->Normal_Segment ($id));
    
    unless ($self->{closed})
    {
        return add_2d ($self->{path}->[$id], $offset_a) if $id == (scalar @{$self->{path}} -1);
        return add_2d ($self->{path}->[$id], $offset_b) if $id == 0;
    }

    return add_2d ($self->{path}->[$id], $offset_a)
        if abs (distance_2d ([0,0], subtract_2d ($offset_a, $offset_b))) < 0.0000000001;

    my $line_a = points_2line (add_2d ($self->Corner_coor ($id-1), $offset_a),
                                add_2d ($self->Corner_coor ($id), $offset_a));

    my $line_b = points_2line (add_2d ($self->Corner_coor ($id), $offset_b),
                                add_2d ($self->Corner_coor ($id+1), $offset_b));

    return line_intersection ($line_a, $line_b);
}

=item Extension_start Extension_end

Get a 2d vector for the extension (if any) from the start of the wall or extrusion

  $vec_2d = $molior->Extension_start;

..or for the end of the wall or extrusion:

  $vec_2d = $molior->Extension_end;

=cut

sub Extension_start
{
    my $self = shift;
    return [0,0] unless defined $self->{extension};
    return scale_2d ($self->Direction_Segment (0), 0 - $self->{extension});
}

sub Extension_end
{
    my $self = shift;
    return [0,0] unless defined $self->{extension};
    return scale_2d ($self->Direction_Segment (scalar @{$self->{path}} -2), $self->{extension});
}

=item Opening_coor Opening_out Opening_in

Get two 3d coordinates locating an opening, order is anticlockwise around path,
bottom-left then top-right:

  ($a, $b) = $molior->Opening_coor ($id_segment, $id_opening);

Similarly, the same but offset for the outer and inner face of the wall:

  ($a, $b) = $molior->Opening_out ($id_segment, $id_opening);
  ($a, $b) = $molior->Opening_in ($id_segment, $id_opening);

=back

=cut

sub Opening_coor
{
    my ($self, $id_segment, $id_opening) = @_;

    my $opening = $self->{openings}->[$id_segment]->[$id_opening];
    my $db = $self->Get_Opening ($opening->{name});
    my $width = $db->{list}->[$opening->{size}]->{width};
    my $height = $db->{list}->[$opening->{size}]->{height};
    my $up = $opening->{up} || 2;
    my $along = $opening->{along};
    my $segment_direction = $self->Direction_Segment ($id_segment);

    my $bottom = $self->{elevation} + $up;
    my $top = $bottom + $height;

    my $a = add_2d ($self->{path}->[$id_segment], scale_2d ($along, $segment_direction));
    my $b = add_2d ($self->{path}->[$id_segment], scale_2d ($along + $width, $segment_direction));

    return ([@{$a}, $bottom], [@{$b}, $top]);
}

sub Opening_out
{
    my ($self, $id_segment, $id_opening) = @_;
    my ($start, $end) = $self->Opening_coor ($id_segment, $id_opening);

    # get a normal vector perpendicular to the wall surface
    my $normal = $self->Normal_Segment ($id_segment);

    # offset opening to place outer face
    my $outer_offset = scale_2d ($self->{outer}, $normal);
    my $l_out = [@{add_2d ([$start->[0], $start->[1]], $outer_offset)}, $start->[2]];
    my $r_out = [@{add_2d ([$end->[0], $end->[1]], $outer_offset)}, $end->[2]];

    return ($l_out, $r_out);
}

sub Opening_in
{
    my ($self, $id_segment, $id_opening) = @_;
    my ($start, $end) = $self->Opening_coor ($id_segment, $id_opening);

    # get a normal vector perpendicular to the wall surface
    my $normal = $self->Normal_Segment ($id_segment);

    my $inner_offset = scale_2d (0-$self->{inner}, $normal);
    my $l_in = [@{add_2d ([$start->[0], $start->[1]], $inner_offset)}, $start->[2]];
    my $r_in = [@{add_2d ([$end->[0], $end->[1]], $inner_offset)}, $end->[2]];

    return ($l_in, $r_in);
}

sub Read_Polyline
{
    my ($self, $name_dxf) = @_;
    my $path_dxf = File::Spec->catfile ($self->Share_dir, $name_dxf);
    if (!-e $path_dxf)
    {
        $path_dxf = File::Spec->catfile ($SHARE_DIR, $name_dxf);
        say STDERR 'Warning: using default for '.$name_dxf;
    }
    use File::DXF;
    use File::DXF::Util;
    use File::DXF::ENTITIES;
    my $dxf = File::DXF->new;
    my $data = File::DXF::Util::read_DXF ($path_dxf);
    $dxf->Process ($data);

    my @report = $dxf->{ENTITIES}->Report (('POLYLINE','VERTEX', 'SEQEND'));
    return File::DXF::ENTITIES::polyline_to_list (@report);
}

=head1 LICENSE

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

=head1 SEE ALSO

L<Urb> L<Molior::DXF> L<molior.pl>

=head1 AUTHOR

Bruno Postle - 2011.

=cut

1;
