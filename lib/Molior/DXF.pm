package Molior::DXF;

use strict;
use warnings;
use lib ('lib', '../lib');
use File::Spec;
use Math::Trig;
use File::ShareDir;
use File::DXF::Math qw /average_3d add_2d points_2line line_intersection subtract_2d scale_2d normalise_2d distance_2d perpendicular_distance/;
use File::DXF::ENTITIES qw /polyface_from_3dfaces/;
use File::DXF::ENTITIES::3DFACE;
use File::DXF::ENTITIES::POINT;
use Urb::Polygon;
use Molior;
use 5.010;

use base 'Molior';
our $VERSION = 0.01;

our $SHARE_DIR = File::ShareDir::module_dir('Molior');
$SHARE_DIR = $ENV{SHARE_DIR} if defined $ENV{SHARE_DIR} and -d $ENV{SHARE_DIR};

=head1 NAME

Molior::DXF - Generate 3D building models in DXF 3DFACE and INSERT format

=head1 SYNOPSIS

  use Molior::DXF;
  use File::DXF;
  use File::DXF::ENTITIES;

  my $molior = Molior::DXF->new;
  $molior->Read ('extrusion.molior');

  my $dxf = File::DXF->new;
  $dxf->{ENTITIES} = File::DXF::ENTITIES->new;
  push @{$dxf->{ENTITIES}}, $molior->Extrusion;

  open DXF, '>extrusion.dxf';
  binmode DXF, ":crlf";
  print DXF $dxf->DXF;
  close DXF;

=head1 DESCRIPTION

Takes simplified descriptions of building parts stored in YAML files and
derives 3D geometry in DXF format.

This is a sub-class of the L<Molior> module

=head1 USAGE

=over

=item Openings_in_use Insert_in_use

Fetch names of all opening types in use, this is a list of file paths to DXF
files to be inserted as BLOCKS:

  my @path_inserts = $molior->Openings_in_use;

Similarly for directly inserted objects:

  push @path_inserts, $molior->Insert_in_use;

=back

=cut

sub Openings_in_use
{
    my $self = shift;
    my %hash;
    for my $segment (@{$self->{openings}})
    {
        for my $opening (@{$segment})
        {
            my $db = $self->Get_Opening ($opening->{name});
            my $filename = $db->{list}->[$opening->{size}]->{file};
            $hash{$filename} = 1;
        }
    }
    my @list;
    for my $file (keys %hash)
    {
        my $path_dxf = File::Spec->catfile ($self->Share_dir, $file);
        if (!-e $path_dxf)
        {
            $path_dxf = File::Spec->catfile ($SHARE_DIR, $file);
            say STDERR 'Warning: using default for '.$file;
        }
        push @list, $path_dxf;
    }
    return @list;
}

=head2 METHODS THAT GENERATE 3D GEOMETRY

=over

=item Insert_insert

Generate a DXF INSERT fragment for a 'molior-insert' object:

  @dxf = $molior->Insert_insert;

=cut

sub Insert_insert
{
    my $self = shift;
    my ($vol, $dirs, $name) = File::Spec->splitpath ($self->Insert_in_use);
    $name =~ s/\.dxf$//xi;
    my @data;
    push @data, [8, 0];
    push @data, [2, $name];
    push @data, [10, $self->{location}->[0]];
    push @data, [20, $self->{location}->[1]];
    push @data, [30, $self->{elevation} + $self->{height}];
    push @data, [50, $self->{rotation} || 0];
    return ({type => 'INSERT', _data => [@data]});
}

=item Stair

Generate DXF 3DFACE entities for a 'molior-stair' object:

  push @dxf, $molior->Stair;

=cut

sub Stair
{
    my $self = shift;
    my @list;
    my $z = $self->{elevation} + $self->{height};
    my $riser = $self->{height} / $self->{risers};

    # we will need to know the longest edge if all corners are in use
    my $id_longest = 0;
    my $length_longest = 0.0;
    for my $id_check (0 .. 3)
    {
        my $id_next = $id_check +1; $id_next = 0 if $id_next == 4;
        my $length_check = distance_2d ($self->{path}->[$id_check], $self->{path}->[$id_next]);
        if ($length_check > $length_longest)
        {
            $id_longest = $id_check; $length_longest = $length_check;
        }
    }

    # take each segment of room in turn, stepping down anti-clockwise
    for my $id_segment ($self->{corners_in_use}->[-1] .. $self->{corners_in_use}->[-1] + 3)
    {
        $id_segment -= scalar @{$self->{path}} if ($id_segment >= scalar @{$self->{path}});

        my $newel_0 = $self->Corner_offset ($id_segment, 0-$self->{width});
        my $newel_1 = $self->Corner_offset ($id_segment +1, 0-$self->{width});

        my $corner_0 = [@{$self->Corner_in ($id_segment)}];
        my $corner_1 = [@{$self->Corner_in ($id_segment +1)}];

        my $width_vec = scale_2d ($self->{width} - $self->{inner}, $self->Normal_Segment ($id_segment));
        my $going_vec = scale_2d ($self->{going}, $self->Direction_Segment ($id_segment));

        my $width_normal = normalise_2d ($width_vec);
        my $going_normal = normalise_2d ($going_vec);

        my $width_vec_1 = scale_2d ($self->{width} - $self->{inner}, $self->Normal_Segment ($id_segment +1));

        # we can't step down when corners are in use for doors
        my $start_in_use = $self->_is_in_use ($id_segment);
        my $end_in_use = $self->_is_in_use ($id_segment +1);

        my $nosing = {closed => 0, type => 'molior-extrusion', colour => 43,
            elevation => undef, height => 0.0, path => [], name => $self->{name},
            cap => [[0,1,2,3]], profile => [[0.0,0.0],[0.0,-0.025],[0.03,-0.025],[0.03,0.0]]};
        bless $nosing, 'Molior::DXF';

        my $a1 = $newel_0;

        # straight runs
        if ($start_in_use and $end_in_use and not (scalar @{$self->{corners_in_use}} == 4 and $id_segment == $id_longest))
        {
            $z = $self->{elevation} + $self->{height};

            # draw a rectangular bit of landing
            push @list, _box ($newel_0, add_2d ($newel_0, $width_vec),
                add_2d ($newel_1, $width_vec), $newel_1, $z, $z-$riser);

            $nosing->{elevation} = $z;
            $nosing->{path} = [$newel_1, $newel_0];
            push @list, $nosing->Extrusion;
        }
        else
        {
            until (distance_2d ($a1, $newel_1) < $self->{going} or ($z-$riser-$riser+0.001 < $self->{elevation}))
            {
                $z -= $riser if ($z - $riser + 0.001 >= $self->{elevation});

                # draw a step
                my $b1 = add_2d ($a1, $width_vec);
                my $c1 = add_2d ($b1, $going_vec);
                my $d1 = add_2d ($a1, $going_vec);
                push @list, _step ($a1, $b1, $c1, $d1, $z, $z-$riser)
                    if ($self->{level} or $z >= $self->{elevation});

                $nosing->{elevation} = $z;
                $nosing->{path} = [$c1, $d1, $a1, $b1];
                push @list, $nosing->Extrusion;

                $a1 = add_2d ($a1, $going_vec);

                # bannister
                my $w = scale_2d (0.025, $width_normal);
                my $g = scale_2d (0.025, $going_normal);
                my $offset = scale_2d (0.5, $going_vec);

                $b1 = subtract_2d ($a1, $g);
                $c1 = add_2d ($b1, $w);
                $d1 = add_2d ($c1, $g);

                push @list, _box ($a1, $b1, $c1, $d1, $z+0.9, $z, 43);

                my $a2 = subtract_2d ($a1, $offset);
                my $b2 = subtract_2d ($b1, $offset);
                my $c2 = subtract_2d ($c1, $offset);
                my $d2 = subtract_2d ($d1, $offset);

                push @list, _box ($a2, $b2, $c2, $d2, $z+0.9 + ($riser/2), $z, 43);

                # handrail
                my $w1 = scale_2d (0.045, $width_normal);
                my $w2 = scale_2d (-0.02, $width_normal);
                
                my $a3 = add_2d ($a1,$w1);
                my $b3 = add_2d ($a1,$w2);
                my $c3 = subtract_2d ($b3, $going_vec);
                my $d3 = subtract_2d ($a3, $going_vec);

                my @tmp;
                push @tmp, _vertical ([@{$a3}, $z+0.85], [@{$b3}, $z+0.90], 43);
                push @tmp, _vertical ([@{$c3}, $z+0.85+$riser], [@{$d3}, $z+0.90+$riser], 43);
                push @tmp, _3dface ([@{$a3}, $z+0.85], [@{$d3}, $z+0.85+$riser], [@{$c3}, $z+0.85+$riser], [@{$b3}, $z+0.85], 43);
                push @tmp, _3dface ([@{$a3}, $z+0.90], [@{$b3}, $z+0.90], [@{$c3}, $z+0.90+$riser], [@{$d3}, $z+0.90+$riser], 43);
                push @tmp, _3dface ([@{$a3}, $z+0.85], [@{$a3}, $z+0.90], [@{$d3}, $z+0.90+$riser], [@{$d3}, $z+0.85+$riser], 43);
                push @tmp, _3dface ([@{$b3}, $z+0.90], [@{$b3}, $z+0.85], [@{$c3}, $z+0.85+$riser], [@{$c3}, $z+0.90+$riser], 43);
                push @list, polyface_from_3dfaces (@tmp);
            }

            $z -= $riser if ($z - $riser + 0.001 >= $self->{elevation});

            # fill odd bit of straight run
            my $b1 = add_2d ($a1, $width_vec);
            my $c1 = add_2d ($newel_1, $width_vec);
            push @list, _box ($a1, $b1, $c1, $newel_1, $z, $z-$riser)
                if ($self->{level} or $z >= $self->{elevation});

            $nosing->{elevation} = $z;
            $nosing->{path} = [$newel_1, $a1];
            push @list, $nosing->Extrusion;

            $z = $self->{elevation} + $self->{height} if ($end_in_use);
        }
        # newel post top
        my $w = scale_2d (0.05, $width_normal);
        my $g = scale_2d (0.05, $going_normal);

        my $a2 = add_2d (subtract_2d ($newel_1, $w), $g);
        my $b2 = subtract_2d (subtract_2d ($newel_1, $w), $g);
        my $c2 = subtract_2d (add_2d ($newel_1, $w), $g);
        my $d2 = add_2d (add_2d ($newel_1, $w), $g);

        push @list, _box ($a2, $b2, $c2, $d2, $z+1.2, $z, 43);
        my $z_save = $z;

        # corner
        my $b1 = add_2d ($newel_1, $width_vec);
        my $d1 = add_2d ($newel_1, $width_vec_1);

        my $span = distance_2d ($b1, $d1) /2;
        my $winders = int ($span / $self->{going});

        if ($end_in_use)
        {
            push @list, _box ($newel_1, $b1, $corner_1, $d1, $z, $z-$riser);

            $nosing->{elevation} = $z;
            $nosing->{path} = [$d1, $newel_1];
            push @list, $nosing->Extrusion;
        }
        elsif ($winders > 3) # draw winders
        {
            my $c1 = add_2d (scale_2d (0.6,$b1), scale_2d (0.4,$corner_1));
            my $e1 = add_2d (scale_2d (0.4,$corner_1), scale_2d (0.6,$d1));
            push @list, _step ($b1, $b1, $c1, $newel_1, $z, $z-$riser)
                if ($self->{level} or $z >= $self->{elevation});

            $nosing->{elevation} = $z;
            $nosing->{path} = [$c1, $newel_1];
            push @list, $nosing->Extrusion;

            $z -= $riser if ($z - $riser + 0.001 >= $self->{elevation});

            push @list, _step ($c1, $c1, $corner_1, $newel_1, $z, $z-$riser)
                if ($self->{level} or $z >= $self->{elevation});

            $nosing->{elevation} = $z;
            $nosing->{path} = [$corner_1, $newel_1];
            push @list, $nosing->Extrusion;

            $z -= $riser if ($z - $riser + 0.001 >= $self->{elevation});

            push @list, _step ($corner_1, $corner_1, $e1, $newel_1, $z, $z-$riser)
                if ($self->{level} or $z >= $self->{elevation});

            $nosing->{elevation} = $z;
            $nosing->{path} = [$e1, $newel_1];
            push @list, $nosing->Extrusion;

            $z -= $riser if ($z - $riser + 0.001 >= $self->{elevation});

            push @list, _step ($e1, $e1, $d1, $newel_1, $z, $z-$riser)
                if ($self->{level} or $z >= $self->{elevation});

            $nosing->{elevation} = $z;
            $nosing->{path} = [$d1, $newel_1];
            push @list, $nosing->Extrusion;
        }
        elsif ($winders == 3)
        {
            my $c1 = add_2d (scale_2d (0.4,$b1), scale_2d (0.6,$corner_1));
            my $e1 = add_2d (scale_2d (0.6,$corner_1), scale_2d (0.4,$d1));
            push @list, _step ($b1, $b1, $c1, $newel_1, $z, $z-$riser)
                if ($self->{level} or $z >= $self->{elevation});

            $nosing->{elevation} = $z;
            $nosing->{path} = [$c1, $newel_1];
            push @list, $nosing->Extrusion;

            $z -= $riser if ($z - $riser + 0.001 >= $self->{elevation});

            push @list, _step ($c1, $corner_1, $e1, $newel_1, $z, $z-$riser)
                if ($self->{level} or $z >= $self->{elevation});

            $nosing->{elevation} = $z;
            $nosing->{path} = [$e1, $newel_1];
            push @list, $nosing->Extrusion;

            $z -= $riser if ($z - $riser + 0.001 >= $self->{elevation});

            push @list, _step ($e1, $e1, $d1, $newel_1, $z, $z-$riser)
                if ($self->{level} or $z >= $self->{elevation});

            $nosing->{elevation} = $z;
            $nosing->{path} = [$d1, $newel_1];
            push @list, $nosing->Extrusion;
        }
        else
        {
            push @list, _step ($b1, $b1, $corner_1, $newel_1, $z, $z-$riser)
                if ($self->{level} or $z >= $self->{elevation});

            $nosing->{elevation} = $z;
            $nosing->{path} = [$corner_1, $newel_1];
            push @list, $nosing->Extrusion;

            $z -= $riser if ($z - $riser + 0.001 >= $self->{elevation});

            push @list, _step ($corner_1, $corner_1, $d1, $newel_1, $z, $z-$riser)
                if ($self->{level} or $z >= $self->{elevation});

            $nosing->{elevation} = $z;
            $nosing->{path} = [$d1, $newel_1];
            push @list, $nosing->Extrusion;
        }
        # newel bottom
        push @list, _box ($a2, $b2, $c2, $d2, $z_save, $z -0.5, 43);
    }
    return @list;
}

sub _is_in_use
{
    my $self = shift;
    my $id = shift;
    $id -= scalar @{$self->{path}} if ($id >= scalar @{$self->{path}});
    my $lookup;
    for my $used (@{$self->{corners_in_use}})
    {
        $used -= scalar @{$self->{path}} if ($used >= scalar @{$self->{path}});
        $lookup->{$used} = 1
    }
    return 1 if defined $lookup->{$id};
    return 0;
}

=item Floor Ceiling Roof Monopitch Duopitch

(These all generate geometry for quadrilateral elements)

Generate DXF 3DFACE entities for a 'molior-floor' object:

  push @dxf, $molior->Floor;

..or a 'molior-ceiling' object:

  push @dxf, $molior->Ceiling;

..or for 'molior-roof' objects:

  push @dxf, $molior->Roof;
  $gradient = 1/3;
  push @dxf, $molior->Monopitch (0, 1, $gradient);
  push @dxf, $molior->Duopitch (2, 1, $gradient);

first parameter is number of edge to be the eave (0, 1, 2 or 3)
second parameter is number of gables

=cut

sub Floor
{
    my $self = shift;
    my $colour = $self->{_style}->{colour}->{floor}->{$self->{name}} || $self->{colour} || undef;
    return 0 unless $self->Is_Quadrilateral;
    my @abcd = map {$self->Corner_in ($_)} (0 .. 3);
    my $bot = $self->{elevation};
    my $top = $bot + $self->{floor};

    return _box (@abcd, $top, $bot, $colour);
}

sub Ceiling
{
    my $self = shift;
    my $colour = $self->{_style}->{colour}->{ceiling}->{$self->{name}} || $self->{colour} || undef;
    return 0 unless $self->Is_Quadrilateral;
    my $top = $self->{elevation} + $self->{height};
    my $bot = $top - $self->{ceiling};
    if (defined $self->{intermittent})
    {
        my @list;
        for ($self->Beams)
        {
             push @list, _box_faces (@{$_}, $top, $bot, $colour)
        }
        return polyface_from_3dfaces (@list);
    }
    else
    {
        my @abcd = map {$self->Corner_in ($_)} (0 .. 3);

        return _box (@abcd, $top, $bot, $colour);
    }
}

sub Light
{
    my $self = shift;
    return 0 unless $self->Is_Quadrilateral;
    my $z = $self->{elevation} + $self->{height} - 0.5;
    my @abcd = map {[@{$self->Corner_in ($_)},$z]} (0 .. 3);
    my $a = average_3d (@abcd);
    my @data;
    push @data, [8, 'LIGHTS'];
    push @data, [10, $a->[0]];
    push @data, [20, $a->[1]];
    push @data, [30, $a->[2]];
    push @data, [62, 51];
    return ({type => 'POINT', _data => [@data]});
}

sub Roof
{
    my $self = shift;
    my $gradient = $self->{_style}->{roof_gradient};
    my @result;
    for my $element (@{$self->{elements}})
    {
        my $gable;
        ($self->{path}, $gable) = @{$element};
        push @result, $self->Duopitch  (0, 0, $gradient) if $gable eq '0000';
        push @result, $self->Monopitch (2, 0, $gradient) if $gable eq '1000';
        push @result, $self->Monopitch (3, 0, $gradient) if $gable eq '0100';
        push @result, $self->Monopitch (0, 0, $gradient) if $gable eq '0010';
        push @result, $self->Monopitch (1, 0, $gradient) if $gable eq '0001';
        push @result, $self->Duopitch  (1, 2, $gradient) if $gable eq '1010';
        push @result, $self->Duopitch  (0, 2, $gradient) if $gable eq '0101';
        push @result, $self->Monopitch (2, 1, $gradient) if $gable eq '1100';
        push @result, $self->Monopitch (3, 1, $gradient) if $gable eq '0110';
        push @result, $self->Monopitch (0, 1, $gradient) if $gable eq '0011';
        push @result, $self->Monopitch (1, 1, $gradient) if $gable eq '1001';
        push @result, $self->Monopitch (0, 2, $gradient) if $gable eq '0111';
        push @result, $self->Monopitch (1, 2, $gradient) if $gable eq '1011';
        push @result, $self->Monopitch (2, 2, $gradient) if $gable eq '1101';
        push @result, $self->Monopitch (3, 2, $gradient) if $gable eq '1110';
    }
    return @result;
}

# requires a four sided quadrilateral
# first parameter is number of edge to be the eave (0, 1, 2 or 3)
# second parameter is number of gables
# third parameter is gradient (0.0 is horizontal, 1.0 is 45 degrees, etc...)

sub Monopitch
{
    my $self = shift;
    my $colour = $self->{_style}->{colour}->{roof}->{top} || $self->{colour} || undef;
    my $colour_gable = $self->{_style}->{colour}->{roof}->{gable} || $self->{colour_gable} || undef;
    my $edge = shift || 0;
    my $gables = shift || 0;
    my $gradient = shift || (75/225);
    return 0 unless $self->Is_Quadrilateral;

    my @list;
    my ($a, $b, $c, $d) = map {$self->Corner_out ($_)} (0 + $edge .. 3 + $edge);
    my ($a_in, $b_in, $c_in, $d_in) = map {$self->Corner_in ($_)} (0 + $edge .. 3 + $edge);
    my $bot = $self->{elevation} + $self->{height};

    my $line_front = points_2line ($a, $b);
    my $line_back = points_2line ($d, $c);

    my $left_apex = line_intersection ($line_back, points_2line ($a, $a_in));
    my $right_apex = line_intersection ($line_back, points_2line ($b, $b_in));

    $left_apex = $d if $gables > 0;
    $right_apex = $c if ($gables > 1 || $gables < 0);

    my $ridge_vector = normalise_2d (subtract_2d ($left_apex, $right_apex));
    my $ridge_vector_gabled = normalise_2d (subtract_2d ($d, $c));

    if (distance_2d ($ridge_vector, $ridge_vector_gabled) > 1)
    {
        say STDERR 'ridge inverts, flipping';
        return $self->Duopitch ($edge+1, -1, $gradient) if $gables == 0;
        return $self->Monopitch ($edge+1, -1, $gradient) if $gables == 1;
        return $self->Monopitch ($edge-1, 1, $gradient) if $gables == -1;
    }

    my $dist_c = perpendicular_distance ($line_front, $right_apex);
    my $top_c = $bot + ($gradient * $dist_c);
    my $dist_d = perpendicular_distance ($line_front, $left_apex);
    my $top_d = $bot + ($gradient * $dist_d);

    # top surface
    push @list, _3dface ([@{$a}, $bot],
                         [@{$b}, $bot],
                         [@{$right_apex}, $top_c],
                         [@{$left_apex}, $top_d], $colour);

    # bottom surface
    push @list, _3dface ([@{$d}, $bot],
                         [@{$c}, $bot],
                         [@{$b}, $bot],
                         [@{$a}, $bot], $colour);

    my $colour_local = $colour;
    $colour_local = $colour_gable if $gables > 0;

    # left wedge
    push @list, _3dface ([@{$left_apex}, $top_d],
                         [@{$d}, $bot],
                         [@{$a}, $bot],
                         [@{$a}, $bot], $colour_local);

    $colour_local = $colour;
    $colour_local = $colour_gable if ($gables > 1 || $gables < 0);

    # right wedge
    push @list, _3dface ([@{$right_apex}, $top_c],
                         [@{$b}, $bot],
                         [@{$b}, $bot],
                         [@{$c}, $bot], $colour_local);

    # back
    push @list, _3dface ([@{$right_apex}, $top_c],
                         [@{$c}, $bot],
                         [@{$d}, $bot],
                         [@{$left_apex}, $top_d], $colour_gable);

    return polyface_from_3dfaces (@list);
}

sub Duopitch
{
    my $self = shift;
    my $colour = $self->{_style}->{colour}->{roof}->{top} || $self->{colour} || undef;
    my $colour_gable = $self->{_style}->{colour}->{roof}->{gable} || $self->{colour_gable} || undef;
    my $edge = shift || 0;
    my $gables = shift || 0;
    my $gradient = shift || (75/225);
    return 0 unless $self->Is_Quadrilateral;

    my @list;
    my ($a, $b, $c, $d) = map {$self->Corner_out ($_)} (0 + $edge .. 3 + $edge);
    my ($a_in, $b_in, $c_in, $d_in) = map {$self->Corner_in ($_)} (0 + $edge .. 3 + $edge);
    my $bot = $self->{elevation} + $self->{height};

    my $line_front = points_2line ($a, $b);
    my $line_back = points_2line ($d, $c);

    my $left_apex = line_intersection (points_2line ($a, $a_in), points_2line ($d, $d_in));
    my $right_apex = line_intersection (points_2line ($b, $b_in), points_2line ($c, $c_in));

    my $line_ridge = points_2line ($left_apex, $right_apex);

    my $left_gabled = line_intersection ($line_ridge, points_2line ($a, $d));
    my $right_gabled = line_intersection ($line_ridge, points_2line ($b, $c));

    $left_apex = $left_gabled if $gables > 0;
    $right_apex = $right_gabled if ($gables > 1 || $gables < 0);

    my $ridge_vector = normalise_2d (subtract_2d ($left_apex, $right_apex));
    my $ridge_vector_gabled = normalise_2d (subtract_2d ($left_gabled, $right_gabled));

    if (distance_2d ($ridge_vector, $ridge_vector_gabled) > 1)
    {
        say STDERR 'ridge inverts, flipping';
        return $self->Duopitch ($edge+1, 0, $gradient) if $gables == 0;
        return $self->Monopitch ($edge+1, 0, $gradient) if $gables == 1;
        return $self->Monopitch ($edge-1, 0, $gradient) if $gables == -1;
    }

    my $dist_left = perpendicular_distance ($line_front, $left_apex);
    my $top_left = $bot + ($gradient * $dist_left);
    my $dist_right = perpendicular_distance ($line_front, $right_apex);
    my $top_right = $bot + ($gradient * $dist_right);

    # top front surface
    push @list, _3dface ([@{$a}, $bot],
                         [@{$b}, $bot],
                         [@{$right_apex}, $top_right],
                         [@{$left_apex}, $top_left], $colour);
    # top back surface
    push @list, _3dface ([@{$c}, $bot],
                         [@{$d}, $bot],
                         [@{$left_apex}, $top_left],
                         [@{$right_apex}, $top_right], $colour);
    # bottom surface
    push @list, _3dface ([@{$d}, $bot],
                         [@{$c}, $bot],
                         [@{$b}, $bot],
                         [@{$a}, $bot], $colour);

    my $colour_local = $colour;
    $colour_local = $colour_gable if $gables > 0;

    # left wedge
    push @list, _3dface ([@{$d}, $bot],
                         [@{$a}, $bot],
                         [@{$left_apex}, $top_left],
                         [@{$left_apex}, $top_left], $colour_local);

    $colour_local = $colour;
    $colour_local = $colour_gable if ($gables > 1 || $gables < 0);

    # right wedge
    push @list, _3dface ([@{$b}, $bot],
                         [@{$c}, $bot],
                         [@{$right_apex}, $top_right],
                         [@{$right_apex}, $top_right], $colour_local);

    return polyface_from_3dfaces (@list);
}

=item Walls

Generate DXF 3DFACE fragments for walls, a single gap is left for one or more
openings in each segment:

  push @dxf, $molior->Walls;

=cut

sub Walls
{
    my $self = shift;
    my $top = $self->{elevation} + $self->{height};
    my $bot = $self->{elevation};

    my $colour = $self->{_style}->{colour}->{wall}->{$self->{name}} || $self->{colour} || undef;

    my @list;
    my $last_segment = scalar @{$self->{path}} -2;
    $last_segment += 1 if ($self->{closed});

    for my $id_segment (0 .. $last_segment)
    {
        my $segment = $self->{openings}->[$id_segment];

        my $v_out_a = $self->Corner_out ($id_segment);
        my $v_out_b = $self->Corner_out ($id_segment +1);

        my $v_in_a = $self->Corner_in ($id_segment);
        my $v_in_b = $self->Corner_in ($id_segment +1);

        unless ($self->{closed})
        {
            if ($id_segment == 0)
            {
                $v_out_a = add_2d ($v_out_a, $self->Extension_start);
                $v_in_a = add_2d ($v_in_a, $self->Extension_start);
            }
            if ($id_segment == $last_segment)
            {
                $v_out_b = add_2d ($v_out_b, $self->Extension_end);
                $v_in_b = add_2d ($v_in_b, $self->Extension_end);
            }
        }

        if (defined $segment->[0])
        {
            my ($l_out_a, $r_out_a) = $self->Opening_out ($id_segment, 0);
            my ($l_out_b, $r_out_b) = $self->Opening_out ($id_segment, -1);

            my ($l_in_a, $r_in_a) = $self->Opening_in ($id_segment, 0);
            my ($l_in_b, $r_in_b) = $self->Opening_in ($id_segment, -1);

            # draw ends if there are openings
            push @list, _box ($v_out_a, $l_out_a, $l_in_a, $v_in_a, $top, $bot, $colour);
            push @list, _box ($r_out_b, $v_out_b, $v_in_b, $r_in_b, $top, $bot, $colour);

            for my $id_opening (0 .. scalar @{$segment} -1)
            {
                my ($start, $end) = $self->Opening_coor ($id_segment, $id_opening);
                my ($start_out, $end_out) = $self->Opening_out ($id_segment, $id_opening);

                my $db = $self->Get_Opening ($segment->[$id_opening]->{name});
                my $block_name = $db->{list}->[$segment->[$id_opening]->{size}]->{file};
                $block_name =~ s/\.dxf$//xi;

                # drop in a block reference for the opening
                my @data;
                push @data, [8, 0];
                push @data, [2, $block_name];
                push @data, [10, $start_out->[0]];
                push @data, [20, $start_out->[1]];
                push @data, [30, $start_out->[2]];
                push @data, [50, $self->Angle_Segment ($id_segment)];
                push @list, {type => 'INSERT', _data => [@data]};

                my ($l_out, $r_out) = $self->Opening_out ($id_segment, $id_opening);
                my ($l_in, $r_in) = $self->Opening_in ($id_segment, $id_opening);

                # wall above and below opening
                push @list, _box ($l_out, $r_out, $r_in, $l_in, $l_out->[2], $bot, $colour) if $l_out->[2] > $bot;
                push @list, _box ($l_out, $r_out, $r_in, $l_in, $top, $r_out->[2], $colour) if $top > $r_out->[2];

                next if $id_opening == scalar @{$segment} -1;

                my ($l_out_1, $r_out_1) = $self->Opening_out ($id_segment, $id_opening +1);
                my ($l_in_1, $r_in_1) = $self->Opening_in ($id_segment, $id_opening +1);

                # wall inbetween openings
                push @list, _box ($r_out, $l_out_1, $l_in_1, $r_in, $top, $bot, $colour);
            }
        }
        else
        {
            # wall with no openings
            push @list, _box ($v_out_a, $v_out_b, $v_in_b, $v_in_a, $top, $bot, $colour);
        }
    }
    return @list;
}

=item Extrusion

Generate DXF 3DFACE fragments for an extrusion defined in a 'molior-extrusion' object:

  push @dxf, $molior->Extrusion;

=cut

sub Extrusion
{
    my $self = shift;

    my $elevation = $self->{elevation};
    my $height = $self->{height};

    my $colour_default = $self->{_style}->{colour}->{extrusion}->{$self->{name}} || $self->{colour} || undef;

    if (!$self->{closed})
    {
        $self->{path}->[0] = add_2d ($self->{path}->[0], $self->Extension_start);
        $self->{path}->[scalar @{$self->{path}} -1] = add_2d ($self->{path}->[scalar @{$self->{path}} -1], $self->Extension_end);
    }

    if (defined $self->{return})
    {
        unshift @{$self->{path}}, $self->Corner_offset (0, $self->{return});
        push @{$self->{path}}, $self->Corner_offset (scalar @{$self->{path}} -1, $self->{return});
    }

    my $last_segment = scalar @{$self->{path}} -2;
    $last_segment += 1 if ($self->{closed});

    my $profiles = [];
    if (ref $self->{profile})
    {
        $profiles->[0]->[0] = $self->{profile};
        $profiles->[0]->[1]->{colour} = $colour_default;
    }
    else
    {
        $profiles = $self->Read_Polyline ($self->{_style}->{profile}->{$self->{name}}) || return ();
    }

    my @output;
    for my $p (@{$profiles})
    {
        my @list;

        my $profile = $p->[0];
        my $colour = $p->[1]->{colour};
        my $polygon = Urb::Polygon->new (@{$profile}, $profile->[0]);
        my $cap = $polygon->to_triangles;

        # cap the ends of the path
        for my $face (@{$cap})
        {
            if (not $self->{closed} and scalar @{$face} == 4)
            {
                my $p_a = $profile->[$face->[0]];
                my $p_b = $profile->[$face->[1]];
                my $p_c = $profile->[$face->[2]];
                my $p_d = $profile->[$face->[3]];

                my $a1 = $self->Corner_offset (0, $p_a->[0]);
                my $b1 = $self->Corner_offset (0, $p_b->[0]);
                my $c1 = $self->Corner_offset (0, $p_c->[0]);
                my $d1 = $self->Corner_offset (0, $p_d->[0]);

                my $colour_local = $colour_default;
                $colour_local = $colour if defined $colour and $colour > 0;

                push @list, _3dface ([@{$a1}, $p_a->[1] + $elevation + $height],
                                     [@{$b1}, $p_b->[1] + $elevation + $height],
                                     [@{$c1}, $p_c->[1] + $elevation + $height],
                                     [@{$d1}, $p_d->[1] + $elevation + $height], $colour_local);

                $a1 = $self->Corner_offset ($last_segment +1, $p_a->[0]);
                $b1 = $self->Corner_offset ($last_segment +1, $p_b->[0]);
                $c1 = $self->Corner_offset ($last_segment +1, $p_c->[0]);
                $d1 = $self->Corner_offset ($last_segment +1, $p_d->[0]);

                push @list, _3dface ([@{$d1}, $p_d->[1] + $elevation + $height],
                                     [@{$c1}, $p_c->[1] + $elevation + $height],
                                     [@{$b1}, $p_b->[1] + $elevation + $height],
                                     [@{$a1}, $p_a->[1] + $elevation + $height], $colour_local);
            }
        }

        for my $id_segment (0 .. $last_segment)
        {
            for my $id_point ( -1 .. scalar @{$profile} -2)
            {
                my $m = $profile->[$id_point];
                my $n = $profile->[$id_point +1];

                my $a1 = $self->Corner_offset ($id_segment, $n->[0]);
                my $b1 = $self->Corner_offset ($id_segment, $m->[0]);
                my $c1 = $self->Corner_offset ($id_segment +1, $m->[0]);
                my $d1 = $self->Corner_offset ($id_segment +1, $n->[0]);

                my $colour_local = $colour_default;
                $colour_local = $colour if defined $colour and $colour > 0;

                push @list, _3dface ([@{$a1}, $n->[1] + $elevation + $height],
                                     [@{$b1}, $m->[1] + $elevation + $height],
                                     [@{$c1}, $m->[1] + $elevation + $height],
                                     [@{$d1}, $n->[1] + $elevation + $height], $colour_local);
            }
        }
        push @output, polyface_from_3dfaces (@list);
    }
    return @output;
}

=item Intermittent

Generate DXF 3DFACE fragments for an intermittent extrusion defined in a 'molior-intermittent' object:

  push @dxf, $molior->Intermittent;

=back

=cut

sub Intermittent
{
    my $self = shift;
    my @list;

    my $elevation = $self->{elevation};
    my $height = $self->{height};
    my $colour = $self->{colour} || undef;
    my $colours = $self->{colours} || undef;

    my $last_segment = scalar @{$self->{path}} -2;
    $last_segment += 1 if ($self->{closed});

    for my $id_segment (0 .. $last_segment)
    {
         my $a1 = $self->Corner_offset ($id_segment, [0,0]);
         my $d1 = $self->Corner_offset ($id_segment +1, [0,0]);
         if (not $self->{closed} and $id_segment == 0)
         {
             $a1 = add_2d ($a1, $self->Extension_start);
         }
         if (not $self->{closed} and $id_segment == $last_segment)
         {
             $d1 = add_2d ($d1, $self->Extension_end);
         }
         my $vector_segment = subtract_2d ($d1, $a1);
         my $length_segment = distance_2d ($d1, $a1);
         my $vector_interval = scale_2d ($self->{interval}, normalise_2d ($vector_segment));
         my $vector_solid = scale_2d ($self->{solid}, normalise_2d ($vector_segment));
         for (0 .. int ($length_segment/$self->{interval}))
         {
             my $end = add_2d ($a1, $vector_solid);
             my $molior = {closed => 0, type => 'molior-extrusion', colour => $colour, colours => $colours,
                   elevation => $elevation, height => $height, path => [$a1, $end], name => $self->{name},
                   cap => $self->{cap}, profile => $self->{profile}};
             bless $molior, 'Molior::DXF';
             push @list, $molior->Extrusion;

             $a1 = add_2d ($a1,$vector_interval);
         }
    }
    return @list;
}

# UTILITY FUNCTIONS

# draw a quad face
# generally interpreted as two triangles: a,b,d and b,c,d

sub _3dface
{
    my ($a, $b, $c, $d, $colour) = @_;
    my @data;
    push @data, [8, 0];

    push @data, [10, $a->[0]];
    push @data, [20, $a->[1]];
    push @data, [30, $a->[2]];

    push @data, [11, $b->[0]];
    push @data, [21, $b->[1]];
    push @data, [31, $b->[2]];

    push @data, [12, $c->[0]];
    push @data, [22, $c->[1]];
    push @data, [32, $c->[2]];

    push @data, [13, $d->[0]];
    push @data, [23, $d->[1]];
    push @data, [33, $d->[2]];

    push @data, [62, $colour] if defined $colour;
    my $entity = File::DXF::ENTITIES::3DFACE->new;
    $entity->Process ([@data]);
    return $entity;
}

# draw a vertical quad face

sub _vertical
{
    my ($a, $b, $colour) = @_;
    my @data;
    push @data, [8, 0];

    push @data, [10, $a->[0]];
    push @data, [20, $a->[1]];
    push @data, [30, $a->[2]];

    push @data, [11, $b->[0]];
    push @data, [21, $b->[1]];
    push @data, [31, $a->[2]];

    push @data, [12, $b->[0]];
    push @data, [22, $b->[1]];
    push @data, [32, $b->[2]];

    push @data, [13, $a->[0]];
    push @data, [23, $a->[1]];
    push @data, [33, $b->[2]];

    push @data, [62, $colour] if defined $colour;
    my $entity = File::DXF::ENTITIES::3DFACE->new;
    $entity->Process ([@data]);
    return $entity;
}

# draw a box that is quadrilateral in plan with vertical sides and horizontal top/bottom

sub _box_faces
{
    my ($a1, $b1, $c1, $d1, $top, $bot, $colour) = @_;
    $a1 = [$a1->[0], $a1->[1]];
    $b1 = [$b1->[0], $b1->[1]];
    $c1 = [$c1->[0], $c1->[1]];
    $d1 = [$d1->[0], $d1->[1]];

    my @list;
     
    # top face
    push @list, _3dface ([@{$a1}, $top],
                         [@{$b1}, $top],
                         [@{$c1}, $top],
                         [@{$d1}, $top], $colour);

    # bottom face
    push @list, _3dface ([@{$d1}, $bot],
                         [@{$c1}, $bot],
                         [@{$b1}, $bot],
                         [@{$a1}, $bot], $colour);

    # outer faces
    push @list, _vertical ([@{$a1}, $bot],
                           [@{$b1}, $top], $colour);
    push @list, _vertical ([@{$b1}, $bot],
                           [@{$c1}, $top], $colour);
    push @list, _vertical ([@{$c1}, $bot],
                           [@{$d1}, $top], $colour);
    push @list, _vertical ([@{$d1}, $bot],
                           [@{$a1}, $top], $colour);

    return @list;
}

sub _box
{
    my @list = _box_faces (@_);
    return polyface_from_3dfaces (@list);
}

sub _step
{
    my ($a1, $b1, $c1, $d1, $top, $bot, $colour) = @_;
    $a1 = [$a1->[0], $a1->[1]];
    $b1 = [$b1->[0], $b1->[1]];
    $c1 = [$c1->[0], $c1->[1]];
    $d1 = [$d1->[0], $d1->[1]];

    my @list;
    my $drop = $bot + $bot - $top;
     
    # top face
    push @list, _3dface ([@{$a1}, $top],
                         [@{$b1}, $top],
                         [@{$c1}, $top],
                         [@{$d1}, $top], $colour);

    # bottom face
    push @list, _3dface ([@{$d1}, $drop],
                         [@{$c1}, $drop],
                         [@{$b1}, $bot],
                         [@{$a1}, $bot], $colour);

    # back faces
    push @list, _vertical ([@{$a1}, $bot],
                           [@{$b1}, $top], $colour);

    # wall side
    push @list, _3dface ([@{$b1}, $top],
                         [@{$b1}, $bot],
                         [@{$c1}, $drop],
                         [@{$c1}, $top], $colour);

    # front face
    push @list, _vertical ([@{$c1}, $drop],
                           [@{$d1}, $top], $colour);

    # well side
    push @list, _3dface ([@{$d1}, $top],
                         [@{$d1}, $drop],
                         [@{$a1}, $bot],
                         [@{$a1}, $top], $colour);

    return polyface_from_3dfaces (@list);
}

=head1 LICENSE

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

=head1 SEE ALSO

L<Urb> L<File::DXF> L<molior.pl>

=head1 AUTHOR

Bruno Postle - 2011.

=cut

1;
